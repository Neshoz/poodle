package se.home.nesho.poodle.attendance.summary;

import java.util.List;

import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.AttendanceOverview;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-22.
 */

public interface SummaryMvp {

    interface View {
        void setSummary(List<Child> attendance);
        void showProgress();
        void showClockOutProgress();
        void hideProgress();
        void showConnectionFailed();
        void showClockOutFailed();
        void changeButtonLayout();
        void navigateToCheckIn(Child child);
        Child getTappedChild();
    }

    interface Presenter {
        void loadSummary();
        void onErrorMessageRetryTapped();
        void checkOutChild(Child child);
        void onChildTapped(Child child);
        void onDestroy();
    }
}
