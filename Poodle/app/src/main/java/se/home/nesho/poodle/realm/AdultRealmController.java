package se.home.nesho.poodle.realm;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;
import se.home.nesho.poodle.model.Adult;

/**
 * Created by 3xR on 2017-02-14.
 */

public class AdultRealmController {

    RealmResults<Adult> getAllAdults(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Adult.class).findAll();
    }

    Adult getAdult(Adult adult){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Adult.class).equalTo("socialSecurityNumber", adult.getSocialSecurityNumber()).findFirst();
    }

    public void postAdult(final Adult adult){
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    adult.setAdultId(realm.where(Adult.class).count() + 1);
                    realm.insertOrUpdate(adult);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if(realm != null){
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    if(realm != null){
                        realm.close();
                    }
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void deleteAdult(final Adult adult){
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Adult realmAdult = realm.where(Adult.class).equalTo("socialSecurityNumber", adult.getSocialSecurityNumber()).findFirst();
                    realmAdult.deleteFromRealm();
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if(realm != null){
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    if(realm != null){
                        realm.close();
                    }
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
