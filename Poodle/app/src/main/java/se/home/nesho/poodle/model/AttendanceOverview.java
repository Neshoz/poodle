package se.home.nesho.poodle.model;

/**
 * Created by Nesho on 2017-02-28.
 */

public class AttendanceOverview {
    private String date;
    private String arrival;
    private String clockOut;
    private boolean present;
    private Child child;

    public AttendanceOverview(String date, String arrival, String clockOut, boolean present, Child child) {
        this.date = date;
        this.arrival = arrival;
        this.clockOut = clockOut;
        this.present = present;
        this.child = child;
    }
    public AttendanceOverview(){

    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getArrival() {
        return arrival;
    }
    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
    public boolean isPresent() {
        return present;
    }
    public void setPresent(boolean present) {
        this.present = present;
    }
    public Child getChild() {
        return child;
    }
    public void setChild(Child child) {
        this.child = child;
    }
    public boolean isClockedOut() {
        return clockOut != null;
    }
}