package se.home.nesho.poodle.attendance.absent;

import java.util.List;

import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-17.
 */

public interface AbsentMvp {

    interface AbsentView {
        void setAttendance(List<Child> data);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToCheckIn(Child child);
    }

    interface AbsentPresenter {
        void loadAbsentAttendance();
        void onErrorMessageRetryTapped();
        void onChildTapped(Child child);
        void onDestroy();
    }
}
