package se.home.nesho.poodle.newAdult;

import android.util.Log;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.retrofit.AdultRestController;
import se.home.nesho.poodle.retrofit.ChildRestController;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Rr on 2017-05-11.
 * Create new adult for the specific child and add relation.
 */

public class NewAdultPresenterImpl implements NewAdultMvp.NewAdultPresenter {

    private NewAdultMvp.NewAdultView view;
    private AdultRestController adultRestController;
    private ChildRestController childRestController;
    private Adult adult;

    public NewAdultPresenterImpl(NewAdultMvp.NewAdultView view) {
        this.view = view;
        this.adultRestController = new AdultRestController();
        this.childRestController = new ChildRestController();
    }

    /**
     * When back press, check if the fields are empty.
     * Depend of the return do something.
     * */
    @Override
    public void onBackPressButtonTapped() {
        if (checkFieldsAreEmpty()) {
            view.navigateToChildOverview();
        } else {
            view.showOnBackPressedDialog();
        }
    }

    /**
     * Save new Adult.
     * */
    @Override
    public void onSaveButtonPressed(String childSSN) {
        view.clearAllErrorMessages();

        if(validateInputFields()) {
            view.showSavingNewAdultProgress();
            adult = new Adult(view.getFirstNameField(),view.getLastNameField(),view.getSSNField(),view.getPhoneNumberField(), view.getWorkNumberField(), view.getEmailField(), view.getRelationSpinner().getSelectedItem().toString());

            this.adultRestController.postNewAdult(childSSN,adult, new AdultRestController.OnPostNewAdultCallback() {
                @Override
                public void onPostNewAdultLoaded() {
                    view.hideProgress();
                    view.successCreatingAdult();

                }

                @Override
                public void onPostNewAdultLoadedFailure(Throwable t) {
                    view.hideProgress();
                    view.showErrorSaveAdult();
                }
            });
        } else {
            Log.d("Validation Error", "Something in the validation is incorrect.");
        }
    }


    /**
     * Field validation to check if something is incorrect.
     * */
    private boolean validateInputFields() {
       if (view.getSSNField().equals(Constants.EMPTY)) {
           view.showSSNEmptyError();
           return false;
       } else if (view.getFirstNameField().equals(Constants.EMPTY)) {
           view.showFirstNameError();
           return false;
       } else if (view.getLastNameField().equals(Constants.EMPTY)) {
          view.showLastNameError();
           return false;
       } else if (view.getPhoneNumberField().equals(Constants.EMPTY)) {
          view.showPhoneNumberError();
           return false;
       } else if (view.getWorkNumberField().equals(Constants.EMPTY)) {
           view.showWorkNumberError();
           return false;
       } else if (view.getEmailField().equals(Constants.EMPTY)) {
           view.showEmailError();
           return false;
       } else if (view.getSSNField().length() < 7) {
           view.showSSNShortError();
           return false;
       } else {
           return true;
       }
    }

    /**
     * Check if they fields are empty , will use it to check
     * if the user is doing something when back press.
     * */
    private boolean checkFieldsAreEmpty() {
        if (!view.getSSNField().equals(Constants.EMPTY)) {
            return false;
        } else if (!view.getFirstNameField().equals(Constants.EMPTY)) {
            return false;
        } else if (!view.getLastNameField().equals(Constants.EMPTY)) {
            return false;
        } else if (!view.getPhoneNumberField().equals(Constants.EMPTY)) {
            return false;
        } else if (!view.getWorkNumberField().equals(Constants.EMPTY)) {
            return false;
        } else if (!view.getEmailField().equals(Constants.EMPTY)) {
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onDestroy() {

    }
}
