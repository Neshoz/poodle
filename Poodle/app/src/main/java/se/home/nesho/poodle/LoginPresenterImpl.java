package se.home.nesho.poodle;

import java.util.List;

import se.home.nesho.poodle.model.Section;
import se.home.nesho.poodle.model.Teacher;
import se.home.nesho.poodle.realm.SectionRealmController;
import se.home.nesho.poodle.realm.TeacherRealmController;
import se.home.nesho.poodle.retrofit.SectionRestController;
import se.home.nesho.poodle.retrofit.TeacherRestController;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-01-19.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView view;
    private Interactor interactor;

    private TeacherRealmController teacherRealmController;
    private TeacherRestController teacherRestController;

    private SectionRealmController sectionRealmController;
    private SectionRestController sectionRestController;

    public LoginPresenterImpl(LoginView view, Interactor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.teacherRealmController = new TeacherRealmController();
        this.sectionRealmController = new SectionRealmController();
        this.teacherRestController = new TeacherRestController();
        this.sectionRestController = new SectionRestController();

        if(!this.interactor.isNetworkAvailable()){
            this.view.disablePassword();
        }
    }

    private void validateAgainstCloud() {
        this.view.showAuthenticateProgress();
        teacherRestController.getTeacher(this.view.getSSN(), new TeacherRestController.OnTeacherLoadedCallback() {
            @Override
            public void onTeacherLoaded(Teacher teacher) {
                if(teacher != null && teacher.getSocialSecurityNumber() == view.getSSN()) {
                    sectionRestController.getSection(view.getSection(), new SectionRestController.OnSectionLoadedCallBack() {
                        @Override
                        public void onSectionLoaded(Section section) {
                            if(view.getPassword().equals(section.getPassword())){
                                Constants.CURRENT_SECTION = view.getSection();
                                Constants.LOGGED_IN_TEACHER = teacher;
                                view.hideAuthenticateProgress();
                                view.navigateToPanel();
                            }
                            else{
                                view.hideAuthenticateProgress();
                                view.setPasswordError();
                            }
                        }
                        @Override
                        public void onSectionLoadedFailure(Throwable t) {
                            view.hideAuthenticateProgress();
                            view.showConnectionFailed();
                        }
                    });
                }
                else {
                    view.hideAuthenticateProgress();
                    view.setSSNError();
                }
            }

            @Override
            public void onTeacherLoadedFailure(Throwable t) {
                view.hideAuthenticateProgress();
                view.showConnectionFailed();
            }
        });
    }

    private void validateOffline() {
        this.view.showAuthenticateProgress();

        if(teacherRealmController.getTeacher(this.view.getSSN()) != null && sectionRealmController.getSection(this.view.getSection()) != null) {
            Constants.CURRENT_SECTION = view.getSection();
            this.view.hideAuthenticateProgress();
            this.view.navigateToPanel();
        }
        else {
            if(this.teacherRealmController.getTeacher(this.view.getSSN()) == null) {
                this.view.hideAuthenticateProgress();
                this.view.setSSNError();
            }
            else {
                this.view.hideAuthenticateProgress();
                this.view.showConnectionFailed();
            }
        }
    }

    @Override
    public void validateCredentials() {
        if(this.interactor.isNetworkAvailable()) {
            this.validateAgainstCloud();
        }
        else {
            this.validateOffline();
        }
    }

    /**
     * Make a request with Retrofit and through our own callback, load the result if it was successful, call view method in success and then update the view in the activity through the method "setSections" that
     * we created in the view interface.
     */
    @Override
    public void loadSections() {
        this.view.showProgress();
        sectionRestController.getAllSections(new SectionRestController.OnSectionsLoadedCallback() {
            @Override
            public void onSectionsLoaded(List<Section> sections) {
                view.setSections(sections);
                view.hideProgress();
            }

            @Override
            public void onSectionsLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onErrorMessageRetryTapped() {
        this.loadSections();
    }


    @Override
    public void onDestroy() {
        this.view = null;
    }
}