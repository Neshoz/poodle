package se.home.nesho.poodle.attendance;

/**
 * Created by Nesho on 2017-02-17.
 */

public interface AttendanceMvp {

    interface AttendanceView {

    }

    interface AttendancePresenter {
        void onDestroy();
    }
}