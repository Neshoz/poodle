package se.home.nesho.poodle.attendance.present;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.Present;

/**
 * Created by Nesho on 2017-02-17.
 */

public class PresentFragment extends Fragment implements PresentMvp.PresentView, PresentAdapter.OnSendHomeClickListener {

    private PresentMvp.PresentPresenter presenter;
    private PresentAdapter adapter;
    private ProgressDialog progressDialog;
    private Child tappedChild;
    private Button tappedButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(this.getContext()).inflate(R.layout.fragment_present, container, false);

        this.adapter = new PresentAdapter(this.getContext(), this);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.present_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        this.presenter = new PresentPresenterImpl(this);
        this.presenter.loadPresentAttendance();

        return view;
    }

    @Override
    public void setAttendance(List<Child> attendance) {
        this.adapter.setItems(attendance);
    }

    @Override
    public void showProgress() {
        this.progressDialog = ProgressDialog.show(this.getActivity(), this.getString(R.string.progressDialogTitle), this.getString(R.string.presentProgressDialogMessage), false);
    }

    @Override
    public void showClockOutProgress() {
        this.progressDialog = ProgressDialog.show(this.getContext(), this.getString(R.string.clockOutProgressDialogMessage), "", false);
    }

    @Override
    public void hideClockOutProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onErrorMessageRetryTapped());
        snackbar.show();
    }

    @Override
    public void showClockedOutFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.onClockOutFailedTapped());
        snackbar.show();
    }

    @Override
    public void changeButtonLayout() {
        this.tappedButton.setText(this.getString(R.string.pickedUpChildButtonText));
        this.tappedButton.setTextColor(Color.rgb(0,150,136));
        this.tappedButton.setBackgroundColor(Color.rgb(250, 250, 250));
        this.tappedButton.setEnabled(false);
    }

    @Override
    public Child getTappedChild() {
        return tappedChild;
    }

    @Override
    public void onSendHomeTapped(Child child) {
        tappedChild = child;
        this.presenter.checkOutChild(child);
    }

    @Override
    public void onButtonTapped(Button button) {
        this.tappedButton = button;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
