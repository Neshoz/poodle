package se.home.nesho.poodle.attendance.absent;
import java.util.List;

import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.retrofit.AttendanceRestController;
import se.home.nesho.poodle.util.AppUtils;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-17.
 */

public class AbsentPresenterImpl implements AbsentMvp.AbsentPresenter {

    private AbsentMvp.AbsentView view;
    private AttendanceRestController restController;

    public AbsentPresenterImpl(AbsentMvp.AbsentView view) {
        this.view = view;
        this.restController = new AttendanceRestController();
    }


    @Override
    public void loadAbsentAttendance() {
        this.view.showProgress();
        restController.getDueToArrive(Constants.CURRENT_SECTION, AppUtils.getCurrentDate(), new AttendanceRestController.OnDueToArriveLoadedCallback() {
            @Override
            public void onDueToArriveLoaded(List<Child> data) {
                view.hideProgress();
                view.setAttendance(data);
            }

            @Override
            public void onDueToArriveLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onErrorMessageRetryTapped() {
        this.loadAbsentAttendance();
    }

    @Override
    public void onChildTapped(Child child) {
        this.view.navigateToCheckIn(child);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
