package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Absent;

/**
 * Created by Nesho on 2017-02-23.
 */

public interface AbsentService {

    @GET("absent/{date}")
    Call<List<Absent>> getAllAbsentChildren(@Path("date") String date);

    @GET("absent/{date}/{section}")
    Call<List<Absent>> getAbsentChildrenInSection(@Path("date") String date, @Path("section") String section);

    @POST("absent/post")
    Call<Absent> postAbsentAttendance(@Body Absent absent);
}
