package se.home.nesho.poodle.newAdult;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import se.home.nesho.poodle.R;

/**
 * Created by Rr on 2017-05-11.
 */

public class NewAdultActivity extends Activity implements NewAdultMvp.NewAdultView {

    public static final Intent newIntent(Context caller, String childSSN) {
        Intent intent = new Intent(caller, NewAdultActivity.class);
        intent.putExtra(EXTRA_CHILD_SSN, childSSN);
        return intent;
    }

    private static final String EXTRA_CHILD_SSN = "ssn";
    private NewAdultMvp.NewAdultPresenter presenter;
    private ProgressDialog progressDialog;
    private AppCompatSpinner relationSpinner;
    private TextInputEditText SSNField;
    private TextInputEditText firstNameField;
    private TextInputEditText lastNameField;
    private TextInputEditText phoneNumberField;
    private TextInputEditText workNumberField;
    private TextInputEditText emailField;
    private FloatingActionButton saveBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_adult);
        this.presenter = new NewAdultPresenterImpl(this);

        //XML
        this.SSNField = (TextInputEditText)findViewById(R.id.newAdult_textInput_SSN);
        this.firstNameField = (TextInputEditText)findViewById(R.id.newAdult_textInput_firstName);
        this.lastNameField = (TextInputEditText)findViewById(R.id.newAdult_textInput_lastName);
        this.phoneNumberField = (TextInputEditText)findViewById(R.id.newAdult_textInput_phoneNumber);
        this.workNumberField = (TextInputEditText)findViewById(R.id.newAdult_textInput_workNumber);
        this.emailField = (TextInputEditText)findViewById(R.id.newAdult_textInput_email);
        this.relationSpinner = (AppCompatSpinner)findViewById(R.id.newAdult_spinner_relation);

        this.saveBtn = (FloatingActionButton)findViewById(R.id.newAdult_floatingButton_save);
        this.saveBtn.setOnClickListener( btnView -> this.presenter.onSaveButtonPressed(getIntent().getStringExtra(EXTRA_CHILD_SSN)));

        // Relation spinner, adapter.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.relationship,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.getRelationSpinner().setAdapter(adapter);
    }


    @Override
    public String getSSNField() {
        return this.SSNField.getText().toString();
    }

    @Override
    public String getFirstNameField() {
        return this.firstNameField.getText().toString();
    }

    @Override
    public String getLastNameField() {
        return this.lastNameField.getText().toString();
    }

    @Override
    public String getPhoneNumberField() {
        return this.phoneNumberField.getText().toString();
    }

    @Override
    public String getWorkNumberField() {
        return this.workNumberField.getText().toString();
    }

    @Override
    public String getEmailField() {
        return this.emailField.getText().toString();
    }

    @Override
    public FloatingActionButton getSaveButton() {
        return this.saveBtn;
    }

    @Override
    public AppCompatSpinner getRelationSpinner() {
        return this.relationSpinner;
    }

    @Override
    public void clearAllErrorMessages() {
        this.SSNField.setError(null);
        this.firstNameField.setError(null);
        this.lastNameField.setError(null);
        this.phoneNumberField.setError(null);
        this.workNumberField.setError(null);
        this.emailField.setError(null);
    }

    @Override
    public void showSSNShortError() {
        this.SSNField.setError(getString(R.string.overviewShortSSN));
        this.SSNField.requestFocus();
    }

    @Override
    public void showSSNEmptyError() {
        this.SSNField.setError(getString(R.string.overviewEmptySSN));
        this.SSNField.requestFocus();
    }

    @Override
    public void showFirstNameError() {
        this.firstNameField.setError(getString(R.string.overviewEmptyFirstname));
        this.firstNameField.requestFocus();
    }

    @Override
    public void showLastNameError() {
        this.lastNameField.setError(getString(R.string.overviewEmptyLastname));
        this.lastNameField.requestFocus();
    }

    @Override
    public void showPhoneNumberError() {
        this.phoneNumberField.setError(getString(R.string.overviewEmptyPhoneNumber));
        this.phoneNumberField.requestFocus();
    }

    @Override
    public void showWorkNumberError() {
        this.workNumberField.setError(getString(R.string.overviewEmptyWorkNumber));
        this.workNumberField.requestFocus();
    }

    @Override
    public void showEmailError() {
        this.emailField.setError(getString(R.string.overviewEmptyEmail));
        this.emailField.requestFocus();
    }


    @Override
    public void showSavingNewAdultProgress() {
        this.progressDialog = ProgressDialog.show(this,this.getString(R.string.newAdult_dialog_savingAdult),"",true);
    }

    /**
     * Fail to save new adult message.
     * */
    @Override
    public void showErrorSaveAdult() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.newAdult_newAdultContainer), this.getString(R.string.newAdult_failToSave), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onSaveButtonPressed(EXTRA_CHILD_SSN));
        snackbar.show();
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void navigateToChildOverview() {
        super.onBackPressed();
    }

    /**
     * Sucess message after posting a new adult.
     * */
    @Override
    public void successCreatingAdult() {
        Toast.makeText(this,getString(R.string.newAdult_successCreating_message),Toast.LENGTH_LONG).show();
        this.navigateToChildOverview();
    }

    @Override
    public AlertDialog showOnBackPressedDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(this.getString(R.string.overview_Dialog_title_discard))
                .setPositiveButton(this.getString(R.string.overview_Dialog_alternative_confirm), (dialog, which) -> this.navigateToChildOverview())
                .setNegativeButton(this.getString(R.string.overview_Dialog_alternative_cancel), (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onBackPressed() {
        this.presenter.onBackPressButtonTapped();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
