package se.home.nesho.poodle.overview;

import java.util.List;

import se.home.nesho.poodle.model.Child;

/**
 * Created by Rr on 2017-02-20.
 */

public interface OverviewMvp {

    interface OverviewView{
        void setChildren(List<Child> data);
        void showProgress();
        void hideProgress();
        void showConnectionFailed();
        void navigateToMenu();
        void errorNoChildrenFound();
    }

    interface OverviewPresenter{
        void onBackPressedTapped();
        void loadChildren();
        void onRetryTapped();
        void onDestroy();
    }
}
