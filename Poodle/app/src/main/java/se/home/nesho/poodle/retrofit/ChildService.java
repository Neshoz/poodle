package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-01-23.
 */

public interface ChildService {

    @GET("children")
    Call<List<Child>> getAllChildren();

    @GET("children/sections/{sectionName}")
    Call<List<Child>> getChildrenInSection(@Path("sectionName") String sectionName);

    @GET("children/{socialSecurityNumber}/parents")
    Call<List<Adult>> getParents(@Path("socialSecurityNumber") String socialSecurityNumber);

    @GET("children/{socialSecurityNumber}")
    Call<Child> getChild(@Path("socialSecurityNumber") String socialSecurityNumber);
    
    @GET("children/{socialSecurityNumber}/pickupadults")
    Call<List<Adult>> getAllowedPickUpAdults(@Path("socialSecurityNumber") String socialSecurityNumber);

    @POST("children/post")
    Call<Child> postChild(@Body Child child);

    @POST("children/remove")
    Call<Child> deleteChild(@Body Child child);

    @POST("children/update")
    Call<Child> editChild(@Body Child child);
}
