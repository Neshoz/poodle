package se.home.nesho.poodle.overview.overviewchild;


import android.util.Log;
import android.view.View;

import java.util.List;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;
import se.home.nesho.poodle.retrofit.AttendanceRestController;
import se.home.nesho.poodle.retrofit.ChildRestController;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Rr on 2017-02-20.
 */

public class OverviewChildPresenterImpl implements OverviewChildMvp.OverviewChildPresenter {

    private OverviewChildMvp.OverviewChildView view;
    private ChildRestController childRestController;
    private AttendanceRestController attendanceRestController;
    private Child currentChild;
    private boolean isEditMode;

    public OverviewChildPresenterImpl(OverviewChildMvp.OverviewChildView view) {
        this.view = view;
        this.childRestController = new ChildRestController();
        this.attendanceRestController = new AttendanceRestController();
        this.isEditMode = false;
    }

    @Override
    public void loadChildInformation(String SSN) {
        this.view.showReadingCurrentChildInformation();

        // Read current child
        this.childRestController.getChild(SSN, new ChildRestController.onChildLoadedCallback() {
            @Override
            public void onChildLoaded(Child child) {
                currentChild = child;
                view.fillFieldsWithChildInformation(child);

                // Read all adults for the child.
                childRestController.getAllowedPickUpAdults(SSN, new ChildRestController.OnPickUpAdultsLoadedCallback() {
                    @Override
                    public void onAdultsLoaded(List<Adult> adults) {
                        if (adults == null) {
                            view.hideProgress();

                        } else if (adults.size() > 0) {
                            view.hideProgress();
                            view.setAdults(adults);

                        } else if (adults.size() < 0) {
                            view.hideProgress();
                        }
                    }

                    @Override
                    public void onAdultsLoadedFailure() {

                    }
                });

                //Read current child message.
                attendanceRestController.getMessage(SSN, new AttendanceRestController.OnMessageLoadedCallback() {
                    @Override
                    public void onMessageLoaded(ChildMessage message) {
                        view.getChildMessageField().setText(message.getMessage());
                    }

                    @Override
                    public void onMessageLoadedFailure() {
                        view.hideProgress();
                        view.showFailToReadChildMessage();
                    }
                });
            }

            @Override
            public void onChildLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showFailToReadChildInformation();
            }
        });

    }


    /**
     * Check if isEditMode true , show confirm dialog.
     * false , navigate to overview.
     * */
    @Override
    public void onBackPressTapped() {
        if (isEditMode) {
            view.showOnBackPressDialog();
        } else {
            view.navigateToOverview();
        }
    }

    /**
     * Toggle to Edit mode & update gui.
     * */
    @Override
    public void onEditTapped() {
        if(!isEditMode) {
            this.isEditMode= true;
            this.editModeGui();
        }
    }

    @Override
    public void onSaveTapped() {
        if(validateInput()) {
            this.saveChildChanges(currentChild);
        } else {
            Log.d("---Validation","Fail");
        }
    }

    /**
     * Save the new changes.
     * */
    private void saveChildChanges(Child currentChild) {
        view.clearAllErrorMessages();
        view.showSavingChanges();
        currentChild.setFirstName(view.getChildFirstNameField().getText().toString());
        currentChild.setLastName(view.getChildLastNameField().getText().toString());

        //Save message
        ChildMessage childMessage = new ChildMessage(currentChild,view.getChildMessageField().getText().toString());


        this.childRestController.editChild(currentChild, new ChildRestController.OnChildEditedLoadedCallBack() {
            @Override
            public void onChildEditedLoaded() {
                // Save child Message
                attendanceRestController.postOrUpdateMessage(childMessage, new AttendanceRestController.OnMessagePostedCallback() {
                    @Override
                    public void onPostSuccess() {
                        view.hideProgress();
                        isEditMode = false;
                        editModeGui();
                    }

                    @Override
                    public void onPostFailure() {
                        Log.d("","Fail to save child message");
                        view.hideProgress();
                        view.showFailToSaveChildInformation();
                    }
                });
            }

            @Override
            public void ChildEditedLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showFailToSaveChildInformation();
            }
        });

    }

    @Override
    public void onAddNewAdultTapped() {
        this.view.navigateToCreateAdult();
    }

    /**
     * Change gui depend on isEditMode
     * */
    private void editModeGui() {
        if (isEditMode) {
            view.getChildFirstNameField().setEnabled(true);
            view.getChildLastNameField().setEnabled(true);
            view.getChildMessageField().setEnabled(true);
            view.getSaveBtn().setVisibility(View.VISIBLE);
            view.getEditBtn().setVisibility(View.GONE);
        } else {
            view.getChildFirstNameField().setEnabled(false);
            view.getChildLastNameField().setEnabled(false);
            view.getChildMessageField().setEnabled(false);
            view.getSaveBtn().setVisibility(View.GONE);
            view.getEditBtn().setVisibility(View.VISIBLE);
        }
    }

    /**
     * Validate if the first name and last name is not empty.
     * */
    private boolean validateInput() {
        if(view.getChildFirstNameField().getText().toString().trim().equals(Constants.EMPTY)) {
            view.showFirstNameError();
            return false;
        } else if (view.getChildLastNameField().getText().toString().trim().equals(Constants.EMPTY)) {
            view.showLastNameError();
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onDestroy() {
        this.view = null;
    }
}
