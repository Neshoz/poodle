package se.home.nesho.poodle.attendance.present;

import android.util.Log;

import java.util.List;

import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.model.Present;
import se.home.nesho.poodle.retrofit.AttendanceRestController;
import se.home.nesho.poodle.retrofit.PresentRestController;
import se.home.nesho.poodle.util.AppUtils;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-17.
 */

public class PresentPresenterImpl implements PresentMvp.PresentPresenter {

    private PresentMvp.PresentView view;
    private AttendanceRestController attendanceRestController;

    public PresentPresenterImpl(PresentMvp.PresentView view) {
        this.view = view;
        this.attendanceRestController = new AttendanceRestController();
    }

    @Override
    public void loadPresentAttendance() {
        this.view.showProgress();
        Log.d("Date: ", AppUtils.getCurrentDate());
        this.attendanceRestController.getPresentChildren(Constants.CURRENT_SECTION, AppUtils.getCurrentDate(), new AttendanceRestController.OnPresentChildrenLoadedCallback() {
            @Override
            public void onPresentChildrenLoaded(List<Child> data) {
                view.hideProgress();
                view.setAttendance(data);
            }

            @Override
            public void onPresentChildrenLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onErrorMessageRetryTapped() {
        this.loadPresentAttendance();
    }

    @Override
    public void onClockOutFailedTapped() {
        this.checkOutChild(this.view.getTappedChild());
    }

    @Override
    public void checkOutChild(Child child) {
        this.view.showClockOutProgress();
        Log.d("Current time", AppUtils.getCurrentTime());
        this.attendanceRestController.clockOut(child.getSocialSecurityNumber(), AppUtils.getCurrentDate(), new ClockOut(AppUtils.getCurrentTime()), new AttendanceRestController.OnClockOutCallback() {
            @Override
            public void onClockOutSuccess() {
                view.hideClockOutProgress();
                view.changeButtonLayout();
            }

            @Override
            public void onClockOutFailure(Throwable t) {
                view.hideClockOutProgress();
                view.showClockedOutFailed();
            }
        });
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
