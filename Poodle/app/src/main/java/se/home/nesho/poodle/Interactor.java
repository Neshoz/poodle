package se.home.nesho.poodle;

/**
 * Created by Nesho on 2017-02-10.
 */

/**
 * An interface to check if any network is available.
 */
public interface Interactor {
    boolean isNetworkAvailable();
}
