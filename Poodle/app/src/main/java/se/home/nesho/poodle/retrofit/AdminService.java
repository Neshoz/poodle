package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Admin;

/**
 * Created by Nesho on 2017-01-23.
 * In future if we are going to add admin functions from application.
 */

public interface AdminService {
    @GET("admins")
    Call<List<Admin>> getAllAdmins();

    @GET("admins/{username}")
    Call<Admin> getAdmin(@Path("username") String username);

    @POST("admins")
    Call<Admin> postAdmin(@Body Admin admin);

    @PUT("admins/{username}/changepassword")
    Call<Admin> changePassword(@Path("username") String username, String newPassword);
}
