package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Teacher;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class TeacherRestController {

    public void getAllTeachers(final OnTeachersLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TeacherService service = retrofit.create(TeacherService.class);
        Call<List<Teacher>> call = service.getAllTeachers();

        call.enqueue(new Callback<List<Teacher>>() {
            @Override
            public void onResponse(Call<List<Teacher>> call, Response<List<Teacher>> response) {
                List<Teacher> teachers = response.body();
                callback.onTeachersLoaded(teachers);
                for(Teacher teacher : response.body()){
                    Log.d("Retrofit Success: ", teacher.getFirstName());
                }
            }

            @Override
            public void onFailure(Call<List<Teacher>> call, Throwable t) {
                callback.onTeachersLoadedFailure(t);
                Log.d("Retrofit Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getTeacher(long socialSecurityNumber, final OnTeacherLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TeacherService service = retrofit.create(TeacherService.class);
        Call<Teacher> call = service.getTeacher(socialSecurityNumber);

        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                Teacher teacher = response.body();
                callback.onTeacherLoaded(teacher);

                if(teacher != null){
                    Log.d("Retrofit Success: ", teacher.getFirstName());
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                callback.onTeacherLoadedFailure(t);
                Log.d("Retrofit Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void postNewTeacher(Teacher teacher , OnPostTeacherLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TeacherService service = retrofit.create(TeacherService.class);
        Call<Teacher> call = service.postTeacher(teacher);

        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onPostTeacherLoaded();
                    Log.d("Retrofit Success","new teacher posted");
                } else {
                    Log.d("Retrofit","Something went wrong on posting, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                callback.onPostTeacherLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new teacher," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void editTeacher(Teacher teacher , OnEditTeacherLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TeacherService service = retrofit.create(TeacherService.class);
        Call<Teacher> call = service.editTeacher(teacher);

        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onEditTeacherLoaded();
                    Log.d("Retrofit Success","edit teacher.");
                } else {
                    Log.d("Retrofit","Something went wrong on edit, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                callback.onEditTeacherLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to Edit teacher," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void removeTeacher(Teacher teacher, OnRemoveTeacherLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TeacherService service = retrofit.create(TeacherService.class);
        Call<Teacher> call = service.deleteTeacher(teacher);

        call.enqueue(new Callback<Teacher>() {
            @Override
            public void onResponse(Call<Teacher> call, Response<Teacher> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onRemoveTeacherLoaded();
                    Log.d("Retrofit Success","removing teacher.");
                } else {
                    Log.d("Retrofit","Something went wrong on remove, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Teacher> call, Throwable t) {
                callback.onRemoveTeacherLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to remove teacher," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public interface OnTeachersLoadedCallback {
        void onTeachersLoaded(List<Teacher> teachers);
        void onTeachersLoadedFailure(Throwable t);
    }

    public interface OnTeacherLoadedCallback {
        void onTeacherLoaded(Teacher teacher);
        void onTeacherLoadedFailure(Throwable t);
    }

    public interface OnPostTeacherLoadedCallback {
        void onPostTeacherLoaded();
        void onPostTeacherLoadedFailure(Throwable t);
    }

    public interface OnEditTeacherLoadedCallback {
        void onEditTeacherLoaded();
        void onEditTeacherLoadedFailure(Throwable t);
    }

    public interface OnRemoveTeacherLoadedCallback {
        void onRemoveTeacherLoaded();
        void onRemoveTeacherLoadedFailure(Throwable t);
    }

}
