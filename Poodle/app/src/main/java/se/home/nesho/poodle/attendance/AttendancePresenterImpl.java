package se.home.nesho.poodle.attendance;

/**
 * Created by Nesho on 2017-02-17.
 */

public class AttendancePresenterImpl implements AttendanceMvp.AttendancePresenter {

    private AttendanceMvp.AttendanceView view;

    public AttendancePresenterImpl(AttendanceMvp.AttendanceView view) {
        this.view = view;
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
