package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.sql.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Emergency;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class EmergencyRestController {

    public void getAllEmergencies (final OnEmergenciesLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EmergencyService service = retrofit.create(EmergencyService.class);
        Call<List<Emergency>> call = service.getAllEmergencies();

        call.enqueue(new Callback<List<Emergency>>() {
            @Override
            public void onResponse(Call<List<Emergency>> call, Response<List<Emergency>> response) {
                List<Emergency> emergencies = response.body();
                callback.onEmergenciesLoaded(emergencies);
                for (Emergency emergency : response.body()) {
                    Log.d("Retrofit Success: ID: ", String.valueOf(emergency.getEmergencyId()) + " Date:" + emergency.getEmergencyDate());
                }
            }

            @Override
            public void onFailure(Call<List<Emergency>> call, Throwable t) {
                callback.onEmergenciesLoadedFailure(t);
                Log.d("Retrofit Failure: " , t.getMessage());
            }
        });
    }

    public void getSectionByDate(Date date, final OnGetEmergenciesByDateLoadedCallback callback ) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EmergencyService service = retrofit.create(EmergencyService.class);
        Call<Emergency> call = service.getEmergency(date);

        call.enqueue(new Callback<Emergency>() {
            @Override
            public void onResponse(Call<Emergency> call, Response<Emergency> response) {
                Emergency emergency = response.body();
                callback.onGetEmergenciesByDateLoaded(emergency);

                if (emergency != null) {
                    Log.d("Retrofit Sucess: ID: " , String.valueOf(emergency.getEmergencyId()) + " Date:" + emergency.getEmergencyDate());
                }
            }

            @Override
            public void onFailure(Call<Emergency> call, Throwable t) {
                callback.onGetEmergenciesByDateLoadedFailure(t);
                Log.d("Retrofit Failure: " , t.getMessage());
            }
        });
    }

    public void postNewEmergency(Emergency emergency , OnPostNewEmergencyLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EmergencyService service = retrofit.create(EmergencyService.class);
        Call<Emergency> call = service.postEmergency(emergency);

        call.enqueue(new Callback<Emergency>() {
            @Override
            public void onResponse(Call<Emergency> call, Response<Emergency> response) {
                if (response.isSuccessful() && response.code() == 204) {
                    callback.onPostNewEmergencyLoaded();
                    Log.d("Retrofit Success","New Emergency Posted");

                } else {
                    Log.d("Retrofit Loaded Error","Something went wrong Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Emergency> call, Throwable t) {
                callback.onPostNewEmergencyLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to post new emergencymessage." + t.getMessage() + " URL: " + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public interface OnEmergenciesLoadedCallback {
        void onEmergenciesLoaded(List<Emergency> emergencies);
        void onEmergenciesLoadedFailure(Throwable t);
    }

    public interface OnGetEmergenciesByDateLoadedCallback {
        void onGetEmergenciesByDateLoaded(Emergency emergency);
        void onGetEmergenciesByDateLoadedFailure(Throwable t);
    }

    public interface OnPostNewEmergencyLoadedCallback {
        void onPostNewEmergencyLoaded();
        void onPostNewEmergencyLoadedFailure(Throwable t);
    }
}
