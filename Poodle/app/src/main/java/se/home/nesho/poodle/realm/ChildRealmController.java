package se.home.nesho.poodle.realm;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.Section;

/**
 * Created by Nesho on 2017-01-24.
 */

public class ChildRealmController {

    public RealmResults<Child> getAllChildren(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Child.class).findAll();
    }

    public RealmResults<Child> getAllChildrenInSection(Section section){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Child.class).equalTo("sectionName", section.getSectionName()).findAll();
    }

    public RealmResults<Adult> getParents(List<Adult> parents){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Adult.class).beginGroup().equalTo("socialSecurityNumber", parents.get(0).getSocialSecurityNumber()).or().equalTo("socialSecurityNumber", parents.get(1).getSocialSecurityNumber()).endGroup().findAll();
    }

    public Child getChild(Child child){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Child.class).equalTo("socialSecurityNumber", child.getSocialSecurityNumber()).findFirst();
    }

    public void postChild(final Child child){
        try{
            final Realm realmInstance = Realm.getDefaultInstance();
            realmInstance.executeTransactionAsync( realm -> {
                    child.setChildId(realm.where(Child.class).count() + 1);
                    realm.insertOrUpdate(child);
            }, () -> {
                if(realmInstance != null) {
                    realmInstance.close();
                }
            }, (error) -> {
                if(realmInstance != null) {
                    realmInstance.close();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void deleteChild(final Child child){
        try{
            final Realm realmInstance = Realm.getDefaultInstance();
            realmInstance.executeTransactionAsync( realm -> {
                    Child realmChild = realm.where(Child.class).equalTo("socialSecurityNumber", child.getSocialSecurityNumber()).findFirst();
                    realmChild.deleteFromRealm();
            }, () -> {
                if(realmInstance != null) {
                    realmInstance.close();
                }
            }, (error) -> {
                if(realmInstance != null) {
                    realmInstance.close();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
