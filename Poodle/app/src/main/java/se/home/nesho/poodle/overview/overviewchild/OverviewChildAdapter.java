package se.home.nesho.poodle.overview.overviewchild;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Adult;


/**
 * Created by Rr on 2017-02-20.
 * Adapter for RecyclerView.
 * Contains all adults for current child.
 */

public class OverviewChildAdapter  extends RecyclerView.Adapter<OverviewChildAdapter.ViewHolder> {

    private List<Adult> adults;
    private OnAdultItemTappedListener onTappListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView adultName, relationToChild;

        public ViewHolder(View itemView) {
            super(itemView);
            this.adultName = (TextView)itemView.findViewById(R.id.overview_adult_list_item_name);
            this.relationToChild = (TextView)itemView.findViewById(R.id.overview_adult_list_item_relation);
        }
    }

    public OverviewChildAdapter (OnAdultItemTappedListener onTappListener) {
        this.adults = new ArrayList<>();
        this.onTappListener = onTappListener;
    }
    public OverviewChildAdapter() {
        this.adults = new ArrayList<>();
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.overview_adult_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Adult adult = this.adults.get(position);

            holder.adultName.setText(this.adults.get(position).getFirstName() + " " + this.adults.get(position).getLastName());
            holder.relationToChild.setText(this.adults.get(position).getRole());
            holder.itemView.setTag(adult);

        /**
         * Click listener when selecting a adult from list.
         * */
        holder.itemView.setOnClickListener(view -> {
            Adult selectedAdult = (Adult)view.getTag();
            onTappListener.onAdultTapped(selectedAdult);
        });
    }

    @Override
    public int getItemCount() {
        return this.adults.size();
    }

    public void setItems(List<Adult> data) {
        this.adults.clear();
        this.adults.addAll(data);
        this.notifyDataSetChanged();
    }

    public interface OnAdultItemTappedListener {
        void onAdultTapped(Adult adult);
    }
}
