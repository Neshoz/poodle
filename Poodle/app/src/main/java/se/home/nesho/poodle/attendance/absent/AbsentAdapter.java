package se.home.nesho.poodle.attendance.absent;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-17.
 */

public class AbsentAdapter extends RecyclerView.Adapter<AbsentAdapter.ViewHolder> {

    private Context context;
    private List<Child> data;
    private OnChildClickedListener onChildClickedListener;

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView childName;
        private TextView childSSN;

        public ViewHolder(View itemView) {
            super(itemView);
            this.childName = (TextView)itemView.findViewById(R.id.absent_child_name_textview);
            this.childSSN = (TextView)itemView.findViewById(R.id.absent_child_ssn_textview);
        }
    }

    public AbsentAdapter(Context context, OnChildClickedListener onChildClickedListener) {
        this.context = context;
        this.data = new ArrayList<>();
        this.onChildClickedListener = onChildClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.absent_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Child child = this.data.get(position);
        holder.childName.setText(child.getFirstName() + " " + child.getLastName());
        holder.childSSN.setText(child.getSocialSecurityNumber());
        holder.itemView.setTag(child);
        holder.itemView.setOnClickListener(view -> {
            Child clickedChild = (Child) view.getTag();
            onChildClickedListener.onChildClicked(clickedChild);
        });
    }

    public void setItems(List<Child> items) {
        this.data.clear();
        this.data.addAll(items);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    interface OnChildClickedListener {
        void onChildClicked(Child child);
    }
}
