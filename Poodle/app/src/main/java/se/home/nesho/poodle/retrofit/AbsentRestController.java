package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Absent;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-23.
 */

public class AbsentRestController {

    public void getAllAbsent(String date, OnGetAllAbsentLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AbsentService service = retrofit.create(AbsentService.class);
        Call<List<Absent>> call = service.getAllAbsentChildren(date);

        call.enqueue(new Callback<List<Absent>>() {
            @Override
            public void onResponse(Call<List<Absent>> call, Response<List<Absent>> response) {
                List<Absent> absent = response.body();
                callback.onGetAllAbsentLoaded(absent);
                Log.d("Retrofit Success: ", "Success fetching absent children from school.");
            }

            @Override
            public void onFailure(Call<List<Absent>> call, Throwable t) {
                callback.onGetAllAbsentLoadedFailure(t);
                Log.d("Retrofit Failure: ", "Failed fetching absent children from school.");
                t.printStackTrace();
            }
        });
    }

    public void getAbsentChildrenInSection(String date, String section, OnAbsentLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AbsentService service = retrofit.create(AbsentService.class);
        Call<List<Absent>> call = service.getAbsentChildrenInSection(date, section);

        call.enqueue(new Callback<List<Absent>>() {
            @Override
            public void onResponse(Call<List<Absent>> call, Response<List<Absent>> response) {
                List<Absent> absent = response.body();
                callback.onAbsentLoaded(absent);
                Log.d("Retrofit Success: ", "Success fetching absent children in section");
            }

            @Override
            public void onFailure(Call<List<Absent>> call, Throwable t) {
                callback.onAbsentLoadedFailure(t);
                Log.d("Retrofit Failure: ", "Failed fetching absent children in section");
                t.printStackTrace();
            }
        });
    }

    public void postNewAbsent(Absent absent, OnPostAbsentLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AbsentService service = retrofit.create(AbsentService.class);
        Call<Absent> call = service.postAbsentAttendance(absent);

        call.enqueue(new Callback<Absent>() {
            @Override
            public void onResponse(Call<Absent> call, Response<Absent> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onPostAbsentLoaded();
                    Log.d("Retrofit Sucess","new absent posted");
                } else {
                    Log.d("Retrofit","Something went wrong on posting, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Absent> call, Throwable t) {
                callback.onPostAbsentLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new absent," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public interface OnGetAllAbsentLoadedCallback {
        void onGetAllAbsentLoaded(List<Absent> data);
        void onGetAllAbsentLoadedFailure(Throwable t);
    }

    public interface OnAbsentLoadedCallback {
        void onAbsentLoaded(List<Absent> data);
        void onAbsentLoadedFailure(Throwable t);
    }

    public interface OnPostAbsentLoadedCallback {
        void onPostAbsentLoaded();
        void onPostAbsentLoadedFailure(Throwable t);
    }
}
