package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class ChildRestController {

    public void getAllChildren(final onChildrenLoadedCallback callback){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<List<Child>> call = service.getAllChildren();

        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                List<Child> children = response.body();
                callback.onChildrenLoaded(children);
                for (Child child : response.body()){
                    Log.d("Retrofit Success: ", child.getFirstName());
                }
            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                callback.onChildrenLoadedFailure(t);
                Log.d("Retrofit Failure: ", t.getMessage());
            }
        });
    }

    public void getChildrenInSection (final onChildrenInSectionLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<List<Child>> call = service.getChildrenInSection(Constants.CURRENT_SECTION);

        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                if(response.isSuccessful()) {

                    List<Child> data = response.body();

                    if(data != null) {
                        callback.onChildrenInSectionLoaded(data);
                    } else {
                        Log.d("Retrofit","Children in section return null.");
                    }

                }

            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                callback.onChildrenInSectionFailure(t);
                Log.d("Retrofit Failure: " , t.getMessage());
            }
        });
    }

    public void getChild(String socialSecurityNumber, final onChildLoadedCallback callback){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<Child> call = service.getChild(socialSecurityNumber);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                Child child = response.body();
                callback.onChildLoaded(child);

            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                callback.onChildLoadedFailure(t);
                Log.d("Retrofit Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getChildParents(String socialSecurityNumber, final onChildParentsLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<List<Adult>> call = service.getParents(socialSecurityNumber);

        call.enqueue(new Callback<List<Adult>>() {
            @Override
            public void onResponse(Call<List<Adult>> call, Response<List<Adult>> response) {
                List<Adult> childAdults = response.body();
                callback.onChildParentsLoaded(childAdults);
            }

            @Override
            public void onFailure(Call<List<Adult>> call, Throwable t) {
                callback.onChildParentsLoadedFailure(t);
                Log.d("Retrofit Failure: " , t.getMessage());
            }
        });
    }

    public void postNewChild(Child child, OnChildPostedLoadedCallBack callBack) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<Child> call = service.postChild(child);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callBack.onChildPostedLoaded();
                    Log.d("Retrofit Success","new child posted");
                } else {
                    Log.d("Retrofit","Something went wrong on posting, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                callBack.onChildPostedLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new child," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void removeChild(Child child, OnChildRemovedLoadedCallBack callBack) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<Child> call = service.deleteChild(child);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callBack.onChildRemovedLoaded();
                } else {
                    Log.d("Retrofit","Something went wrong on removing, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                callBack.onChildRemovedLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to Remove  child," + t.getMessage());
                t.printStackTrace();
            }
        });

    }

    public void editChild(Child child, OnChildEditedLoadedCallBack callBack) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<Child> call = service.editChild(child);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callBack.onChildEditedLoaded();
                } else {
                    Log.d("Retrofit","Something went wrong on posting changes, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Child> call, Throwable t) {
                callBack.ChildEditedLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new child changes.");
                t.printStackTrace();
            }
        });
    }

    public void getAllowedPickUpAdults(String socialSecurityNumber, OnPickUpAdultsLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ChildService service = retrofit.create(ChildService.class);
        Call<List<Adult>> call = service.getAllowedPickUpAdults(socialSecurityNumber);

        call.enqueue(new Callback<List<Adult>>() {
            @Override
            public void onResponse(Call<List<Adult>> call, Response<List<Adult>> response) {
                List<Adult> adults = response.body();
                Log.d("Retrofit", "Success fetching pick-up adults - " + response.code());
                callback.onAdultsLoaded(adults);
            }

            @Override
            public void onFailure(Call<List<Adult>> call, Throwable t) {
                Log.d("Retrofit", "Failure fetching pick-up adults");
                t.printStackTrace();
                callback.onAdultsLoadedFailure();
            }
        });
    }

    public interface OnPickUpAdultsLoadedCallback {
        void onAdultsLoaded(List<Adult> adults);
        void onAdultsLoadedFailure();
    }

    public interface onChildrenLoadedCallback {
        void onChildrenLoaded(List<Child> children);
        void onChildrenLoadedFailure(Throwable t);
    }

    public interface onChildrenInSectionLoadedCallback {
        void onChildrenInSectionLoaded(List<Child> data);
        void onChildrenInSectionFailure(Throwable t);
    }

    public interface onChildLoadedCallback {
        void onChildLoaded(Child child);
        void onChildLoadedFailure(Throwable t);
    }

    public interface onChildParentsLoadedCallback {
        void onChildParentsLoaded(List<Adult> adults);
        void onChildParentsLoadedFailure(Throwable t);
    }

    public interface OnChildPostedLoadedCallBack {
        void onChildPostedLoaded();
        void onChildPostedLoadedFailure(Throwable t);
    }

    public interface OnChildRemovedLoadedCallBack {
        void onChildRemovedLoaded();
        void onChildRemovedLoadedFailure(Throwable t);
    }

    public interface OnChildEditedLoadedCallBack {
        void onChildEditedLoaded();
        void ChildEditedLoadedFailure(Throwable t);
    }
}
