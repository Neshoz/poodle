package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.model.Present;

/**
 * Created by Nesho on 2017-01-23.
 */

public interface AttendanceService {

    @GET("attendance")
    Call<List<Attendance>> getAllAttendances();

    @GET("attendance/{socialSecurityNumber}")
    Call<List<Attendance>> getAttendanceForChild(@Path("socialSecurityNumber") long socialSecurityNumber);

    @GET("attendance/overview/{date}/{section}")
    Call<List<Child>> getOverview(@Path("date") String date, @Path("section")String section);

    @GET("attendance/{section}/{date}/absent")
    Call<List<Child>> getDueToArrive(@Path("section") String section, @Path("date") String date);

    @GET("attendance/{section}/{date}/present")
    Call<List<Child>> getPresentChildren(@Path("section") String section, @Path("date") String date);

    @GET("attendance/{ssn}/message")
    Call<ChildMessage> getMessage(@Path("ssn") String socialSecurityNumber);

    @POST("attendance/post")
    Call<Attendance> postAttendance(@Body Attendance attendance);

    @POST("attendance/{ssn}/{date}/clockout")
    Call<ClockOut> clockOut(@Path("ssn") String ssn, @Path("date") String date, @Body ClockOut clockOut);

    @POST("attendance/clockin")
    Call<Present> clockIn(@Body Present attendance);

    @POST("attendance/message")
    Call<ChildMessage> postOrUpdateMessage(@Body ChildMessage message);
}
