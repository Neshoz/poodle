package se.home.nesho.poodle;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.support.v7.widget.AppCompatSpinner;

import java.util.List;

import se.home.nesho.poodle.menu.MenuActivity;
import se.home.nesho.poodle.model.Section;
import se.home.nesho.poodle.util.Constants;

public class LoginActivity extends Activity implements LoginView {

    private AppCompatSpinner spinner;
    private TextInputLayout passwordInputLayout;
    private TextInputEditText ssnInput;
    private TextInputEditText passwordInput;
    private LoginPresenter presenter;
    private ArrayAdapter<Section> adapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        spinner = (AppCompatSpinner) findViewById(R.id.sections_spinner);
        ssnInput = (TextInputEditText) findViewById(R.id.login_SSN);
        passwordInput = (TextInputEditText) findViewById(R.id.login_password);
        passwordInputLayout = (TextInputLayout) findViewById(R.id.layout_password_input);
        Button login = (Button) findViewById(R.id.login_submit);

        presenter = new LoginPresenterImpl(this, new UseCase(this));
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        login.setOnClickListener(view -> presenter.validateCredentials());

        presenter.loadSections();
    }

    @Override
    public String getSection() {
        return this.spinner.getSelectedItem().toString();
    }

    @Override
    public long getSSN() {
        long ssnText = 0;

        if(!this.ssnInput.getText().toString().equals(Constants.EMPTY)) {
            ssnText = Long.parseLong(ssnInput.getText().toString());
        }

        return ssnText;
    }

    @Override
    public String getPassword() {
        return this.passwordInput.getText().toString();
    }

    /**
     * This method updates the UI through the presenter. We call presenter.loadSections() when we click our button and in our callback in our Presenter we call view.setSections() - The UI is then updated when they response
     * come back.
     * @param sections the returned list we fetched from our Cloud (Rest API) with Retrofit.
     */
    @Override
    public void setSections(List<Section> sections) {
        this.spinner.setAdapter(adapter);
        this.adapter.addAll(sections);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.progressDialogTitle), this.getString(R.string.progressDialogMessage), true);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showAuthenticateProgress() {
        this.progressDialog = ProgressDialog.show(this, "", getString(R.string.authenticating), false);
    }

    @Override
    public void hideAuthenticateProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_login), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onErrorMessageRetryTapped());
        snackbar.show();
    }

    @Override
    public void setSSNError() {
        this.ssnInput.setError(getString(R.string.SSNError));
    }

    @Override
    public void setPasswordError() {
        this.passwordInput.setError(getString(R.string.passwordError));
    }

    @Override
    public void disablePassword() {
        this.passwordInputLayout.setVisibility(View.GONE);
    }

    @Override
    public void navigateToPanel() {
        startActivity(new Intent(this, MenuActivity.class));
    }

    @Override
    public void onDestroy(){
        this.presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed (){
        this.finishAffinity();
    }
}
