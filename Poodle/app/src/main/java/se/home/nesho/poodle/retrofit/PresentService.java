package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.model.Present;

/**
 * Created by Nesho on 2017-02-23.
 */

public interface PresentService {
    @GET("present/{date}")
    Call<List<Present>> getAllPresentChildren(@Path("date") String date);

    @GET("present/{date}/{section}")
    Call<List<Present>> getPresentChildrenInSection(@Path("date") String date, @Path("section") String section);

    @GET("present/{date}/{section}/{ssn}")
    Call<Present> getPresentChild(@Path("date") String date, @Path("section") String section, @Path("ssn") long socialSecurityNumber);

    @POST("present")
    Call<Present> postPresentAttendance(@Body Present present);

    @POST("present/{date}/{ssn}")
    Call<ClockOut> clockOutChild(@Path("date") String date, @Path("ssn") long socialSecurityNumber, @Body ClockOut clockOutTime);
}
