package se.home.nesho.poodle.overview.overviewchild;

import android.app.AlertDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Rr on 2017-02-20.
 */

public interface OverviewChildMvp {

    interface OverviewChildView {
        //Fields,buttons, text.
        FloatingActionButton getEditBtn();
        FloatingActionButton getSaveBtn();
        EditText getChildMessageField();
        TextView getTitleName();
        TextInputEditText getChildSSNField();
        TextInputEditText getChildFirstNameField();
        TextInputEditText getChildLastNameField();
        void fillFieldsWithChildInformation (Child child);
        void setAdults(List<Adult> data);

        //Error
        void clearAllErrorMessages();
        void showFirstNameError();
        void showLastNameError();
        void showFailToReadChildInformation();
        void showFailToSaveChildInformation();
        void showFailToReadChildMessage();

        //Dialogs & information
        void showReadingCurrentChildInformation();
        void showSavingChanges();
        void hideProgress();
        AlertDialog showOnBackPressDialog();

        //Navigate
        void navigateToCreateAdult();
        void navigateToOverview();

    }

    interface OverviewChildPresenter {
        void onBackPressTapped();
        void onEditTapped();
        void onSaveTapped();
        void onAddNewAdultTapped();
        void loadChildInformation(String SSN);
        void onDestroy();
    }
}
