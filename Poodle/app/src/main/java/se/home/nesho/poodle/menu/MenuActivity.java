package se.home.nesho.poodle.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import se.home.nesho.poodle.LoginActivity;
import se.home.nesho.poodle.R;
import se.home.nesho.poodle.attendance.AttendanceActivity;
import se.home.nesho.poodle.overview.OverviewActivity;

public class MenuActivity extends Activity implements MenuMvp.MenuView {

    private MenuMvp.MenuPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ImageView attendanceImage = (ImageView) findViewById(R.id.attendance_image);
        ImageView overviewImage = (ImageView) findViewById(R.id.overview_image);
        ImageView logoutImage = (ImageView) findViewById(R.id.logout_image);
        ImageView emergencyImage = (ImageView) findViewById(R.id.emergency_image);

        presenter = new MenuPresenterImpl(this);

        attendanceImage.setOnClickListener(view -> presenter.onAttendanceTapped());
        overviewImage.setOnClickListener(view -> presenter.onOverviewTapped());
        logoutImage.setOnClickListener(view -> presenter.onLogoutTapped());
        emergencyImage.setOnClickListener(view -> presenter.onEmergencyTapped());
    }

    @Override
    public void navigateToAttendance() {
        this.startActivity(new Intent(this, AttendanceActivity.class));
    }

    @Override
    public void navigateToOverview() {
        this.startActivity(new Intent(this, OverviewActivity.class));
    }

    @Override
    public void navigateToLogin() {
        this.startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void navigateToEmergency() {
        this.startActivity(new Intent());
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        this.presenter.onLogoutTapped();
        this.presenter.onDestroy();
    }
}