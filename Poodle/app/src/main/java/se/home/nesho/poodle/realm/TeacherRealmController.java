package se.home.nesho.poodle.realm;

import android.util.Log;

import io.realm.Realm;
import io.realm.RealmResults;
import se.home.nesho.poodle.model.Teacher;

/**
 * Created by Nesho on 2017-01-24.
 */

public class TeacherRealmController {

    public RealmResults<Teacher> getAllTeachers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Teacher> realmResults =  realm.where(Teacher.class).findAll();
        realm.close();
        return realmResults;
    }

    public Teacher getTeacher(long SSN){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Teacher.class).equalTo("socialSecurityNumber", SSN).findFirst();
    }

    public void postTeacher(final Teacher teacher) {
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    teacher.setTeacherId(realm.where(Teacher.class).count() + 1);
                    realm.insertOrUpdate(teacher);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if (realm != null) {
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch(Exception ex){
            Log.d("Realm failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void deleteTeacher(final Teacher teacher){
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Teacher realmTeacher = realm.where(Teacher.class).equalTo("socialSecurityNumber", teacher.getSocialSecurityNumber()).findFirst();
                    realmTeacher.deleteFromRealm();
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if (realm != null) {
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void changeFirstName(final Teacher teacher, final String newName) {
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Teacher realmTeacher = realm.where(Teacher.class).equalTo("socialSecurityNumber", teacher.getSocialSecurityNumber()).findFirst();
                    realmTeacher.setFirstName(newName);
                    realm.insertOrUpdate(realmTeacher);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if (realm != null) {
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void changeLastName(final Teacher teacher, final String newName) {
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Teacher realmTeacher = realm.where(Teacher.class).equalTo("socialSecurityNumber", teacher.getSocialSecurityNumber()).findFirst();
                    realmTeacher.setLastName(newName);
                    realm.insertOrUpdate(realmTeacher);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if (realm != null) {
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void changePhoneNumber(final Teacher teacher, final long newNumber) {
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Teacher realmTeacher = realm.where(Teacher.class).equalTo("socialSecurityNumber", teacher.getSocialSecurityNumber()).findFirst();
                    realmTeacher.setPhoneNumber(newNumber);
                    realm.insertOrUpdate(realmTeacher);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if (realm != null) {
                        realm.close();
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void deleteEverything(){
        try{
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(Teacher.class);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if(realm != null){
                        realm.close();
                        Log.d("Success", "Deleted All Teachers");
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    realm.close();
                    Log.d("Realm Failure", error.getMessage());
                    error.printStackTrace();
                }
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
