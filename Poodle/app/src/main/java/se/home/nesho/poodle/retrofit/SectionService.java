package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Section;

/**
 * Created by Nesho on 2017-01-23.
 */

public interface SectionService {

    @GET("sections")
    Call<List<Section>> getAllSections();

    @GET("sections/{sectionName}")
    Call<Section> getSection(@Path("sectionName") String sectionName);

}
