package se.home.nesho.poodle.attendance.summary;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-22.
 */

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.BaseViewHolder> {

    private Context context;
    private List<Child> overview;
    private OnSendHomeClickListener sendHomeClickListener;
    private OnAbsentChildClickListener absentChildClickListener;
    private OnButtonTappedListener buttonTappedListener;
    private static final int TYPE_ABSENT = 0;
    private static final int TYPE_PRESENT = 1;

    public SummaryAdapter(Context context, OnSendHomeClickListener sendHomeClickListener, OnAbsentChildClickListener absentChildClickListener, OnButtonTappedListener buttonTappedListener) {
        this.context = context;
        this.overview = new ArrayList<>();
        this.sendHomeClickListener = sendHomeClickListener;
        this.absentChildClickListener = absentChildClickListener;
        this.buttonTappedListener = buttonTappedListener;
    }
    
    @Override
    public int getItemViewType(int position) {
        return this.overview.get(position).isPresent() ? TYPE_PRESENT : TYPE_ABSENT;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_ABSENT) {
            return new AbsentViewHolder(LayoutInflater.from(context).inflate(R.layout.absent_item, parent, false));
        }
        else {
            return new PresentViewHolder(LayoutInflater.from(context).inflate(R.layout.present_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if(holder instanceof AbsentViewHolder) {
            AbsentViewHolder absentHolder = (AbsentViewHolder) holder;
            Child child = this.overview.get(position);

            absentHolder.childName.setText(child.getFirstName() + " " + child.getLastName());
            absentHolder.childSSN.setText(child.getSocialSecurityNumber());
            absentHolder.itemView.setTag(child);
            absentHolder.itemView.setOnClickListener(view -> {
                Child tappedChild = (Child) view.getTag();
                absentChildClickListener.onAbsentChildTapped(tappedChild);
            });
        }
        else if(holder instanceof PresentViewHolder) {
            PresentViewHolder presentHolder = (PresentViewHolder) holder;
            Child child = this.overview.get(position);

            presentHolder.childName.setText(child.getFirstName() + " " + child.getLastName());
            presentHolder.childSSN.setText(child.getSocialSecurityNumber());
            presentHolder.arrival.setText(String.format(this.context.getString(R.string.arrivalTextView), child.getArrival()));
            presentHolder.sendHomeButton.setTag(child);
            presentHolder.sendHomeButton.setOnClickListener(view -> {
                Child tappedChild = (Child) view.getTag();
                sendHomeClickListener.onSendHomeTapped(tappedChild);
                buttonTappedListener.onButtonTapped(presentHolder.sendHomeButton);
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.overview.size();
    }

    public void setItems(List<Child> data) {
        this.overview.clear();
        this.overview.addAll(data);
        this.notifyDataSetChanged();
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class AbsentViewHolder extends BaseViewHolder {
        private TextView childName;
        private TextView childSSN;

        public AbsentViewHolder(View itemView) {
            super(itemView);
            this.childName = (TextView)itemView.findViewById(R.id.absent_child_name_textview);
            this.childSSN = (TextView)itemView.findViewById(R.id.absent_child_ssn_textview);
        }
    }

    public static class PresentViewHolder extends BaseViewHolder {
        private TextView childName;
        private TextView childSSN;
        private TextView arrival;
        private Button sendHomeButton;

        public PresentViewHolder(View itemView) {
            super(itemView);
            this.childName = (TextView)itemView.findViewById(R.id.present_child_name_textview);
            this.childSSN = (TextView)itemView.findViewById(R.id.present_child_ssn_textview);
            this.arrival = (TextView)itemView.findViewById(R.id.present_child_clockIn_textview);
            this.sendHomeButton = (Button)itemView.findViewById(R.id.clockOut_button);
        }
    }

    public interface OnSendHomeClickListener {
        void onSendHomeTapped(Child child);
    }

    public interface OnAbsentChildClickListener {
        void onAbsentChildTapped(Child child);
    }

    public interface OnButtonTappedListener {
        void onButtonTapped(Button button);
    }

}
