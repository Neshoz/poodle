package se.home.nesho.poodle.attendance.present;

import java.util.List;

import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-17.
 */

public interface PresentMvp {

    interface PresentView {
        void setAttendance(List<Child> children);
        void showProgress();
        void showClockOutProgress();
        void hideClockOutProgress();
        void hideProgress();
        void showConnectionFailed();
        void showClockedOutFailed();
        void changeButtonLayout();
        Child getTappedChild();
    }

    interface PresentPresenter {
        void loadPresentAttendance();
        void onErrorMessageRetryTapped();
        void onClockOutFailedTapped();
        void checkOutChild(Child child);
        void onDestroy();
    }
}
