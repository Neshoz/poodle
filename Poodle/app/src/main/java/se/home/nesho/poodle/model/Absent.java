package se.home.nesho.poodle.model;

import io.realm.RealmObject;

/**
 * Created by Nesho on 2017-02-27.
 */

public class Absent extends RealmObject {
    private long absentId;
    private Section section;
    private Child child;
    private String date;

    public Absent(long absentId, Section section, Child child, String date) {
        this.absentId = absentId;
        this.section = section;
        this.child = child;
        this.date = date;
    }
    public Absent() {

    }


    public long getAbsentId() {
        return absentId;
    }
    public void setAbsentId(long absentId) {
        this.absentId = absentId;
    }
    public Section getSection() {
        return section;
    }
    public void setSection(Section section) {
        this.section = section;
    }
    public Child getChild() {
        return child;
    }
    public void setChild(Child child) {
        this.child = child;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
}
