package se.home.nesho.poodle.model;

import java.io.Serializable;

/**
 * Created by Nesho on 2017-03-01.
 */

public class ClockOut implements Serializable {
    private String time;

    public ClockOut(String time) {
        this.time = time;
    }
    public ClockOut() {

    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
}
