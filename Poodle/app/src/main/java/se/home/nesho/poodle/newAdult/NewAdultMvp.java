package se.home.nesho.poodle.newAdult;

import android.app.AlertDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;

/**
 * Created by Rr on 2017-05-11.
 */

public interface NewAdultMvp {

    interface NewAdultView {
        String getSSNField();
        String getFirstNameField();
        String getLastNameField();
        String getPhoneNumberField();
        String getWorkNumberField();
        String getEmailField();
        FloatingActionButton getSaveButton();
        AppCompatSpinner getRelationSpinner();

        //Error
        void clearAllErrorMessages();
        void showSSNShortError();
        void showSSNEmptyError();
        void showFirstNameError();
        void showLastNameError();
        void showPhoneNumberError();
        void showWorkNumberError();
        void showEmailError();
        void showSavingNewAdultProgress();
        void showErrorSaveAdult();

        void hideProgress();
        void navigateToChildOverview();
        void successCreatingAdult();
        AlertDialog showOnBackPressedDialog();

    }

    interface NewAdultPresenter {
        void onBackPressButtonTapped();
        void onSaveButtonPressed(String childSSN);
        void onDestroy();
    }
}
