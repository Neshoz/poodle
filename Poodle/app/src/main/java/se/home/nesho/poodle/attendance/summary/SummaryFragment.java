package se.home.nesho.poodle.attendance.summary;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.checkin.CheckinActivity;
import se.home.nesho.poodle.model.AttendanceOverview;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-22.
 */

public class SummaryFragment extends Fragment implements SummaryMvp.View, SummaryAdapter.OnSendHomeClickListener, SummaryAdapter.OnAbsentChildClickListener, SummaryAdapter.OnButtonTappedListener {

    private SummaryMvp.Presenter presenter;
    private SummaryAdapter adapter;
    private ProgressDialog progressDialog;
    private Button tappedButton;
    private Child tappedChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.presenter = new SummaryPresenterImpl(this);
        this.presenter.loadSummary();

        return inflater.inflate(R.layout.fragment_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.summary_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        this.adapter = new SummaryAdapter(this.getActivity(), this, this, this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setSummary(List<Child> attendance) {
        this.adapter.setItems(attendance);
    }

    @Override
    public void showProgress() {
        this.progressDialog = ProgressDialog.show(this.getActivity(), this.getString(R.string.progressDialogTitle), this.getString(R.string.summaryProgressDialogMessage), false);
    }

    @Override
    public void showClockOutProgress() {
        this.progressDialog = ProgressDialog.show(this.getContext(), Constants.EMPTY, this.getString(R.string.clockOutProgressDialogMessage), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onErrorMessageRetryTapped());
        snackbar.show();
    }

    @Override
    public void showClockOutFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_LONG)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.checkOutChild(this.tappedChild));
        snackbar.show();
    }

    @Override
    public void changeButtonLayout() {
        this.tappedButton.setText(this.getString(R.string.pickedUpChildButtonText));
        this.tappedButton.setTextColor(Color.rgb(0,150,136));
        this.tappedButton.setBackgroundColor(Color.rgb(250, 250, 250));
        this.tappedButton.setEnabled(false);
    }

    @Override
    public void navigateToCheckIn(Child child) {
        this.startActivity(CheckinActivity.newIntent(this.getActivity(), child.getSocialSecurityNumber()));
    }

    @Override
    public Child getTappedChild() {
        return this.tappedChild;
    }

    @Override
    public void onAbsentChildTapped(Child child) {
        this.presenter.onChildTapped(child);
    }

    @Override
    public void onSendHomeTapped(Child child) {
        this.tappedChild = child;
        this.presenter.checkOutChild(child);
    }

    @Override
    public void onButtonTapped(Button button) {
        this.tappedButton = button;
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
