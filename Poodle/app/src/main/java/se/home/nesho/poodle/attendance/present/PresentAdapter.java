package se.home.nesho.poodle.attendance.present;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.Present;

/**
 * Created by Nesho on 2017-02-17.
 */

public class PresentAdapter extends RecyclerView.Adapter<PresentAdapter.ViewHolder> {

    private Context context;
    private List<Child> presentAttendance;
    private OnSendHomeClickListener listener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView childName;
        private TextView childSSN;
        private TextView clockIn;
        private Button clockOutButton;

        public ViewHolder(View itemView) {
            super(itemView);
            this.childName = (TextView)itemView.findViewById(R.id.present_child_name_textview);
            this.childSSN = (TextView)itemView.findViewById(R.id.present_child_ssn_textview);
            this.clockIn = (TextView)itemView.findViewById(R.id.present_child_clockIn_textview);
            this.clockOutButton = (Button)itemView.findViewById(R.id.clockOut_button);
        }
    }

    public PresentAdapter(Context context, OnSendHomeClickListener listener) {
        this.context = context;
        this.presentAttendance = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.present_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Child child = this.presentAttendance.get(position);

        holder.childName.setText(child.getFirstName() + " " + child.getLastName());
        holder.childSSN.setText(child.getSocialSecurityNumber());
        holder.clockIn.setText(String.format(this.context.getString(R.string.arrivalTextView), child.getArrival()));
        holder.itemView.setTag(child);
        holder.clockOutButton.setTag(child);

        if(child.getClockOut() != null) {
            this.changeButtonLayout(holder.clockOutButton);
        }

        holder.clockOutButton.setOnClickListener(view -> {
            Child tappedChild = (Child) view.getTag();
            listener.onSendHomeTapped(tappedChild);
            listener.onButtonTapped(holder.clockOutButton);
        });
    }

    @Override
    public int getItemCount() {
        return this.presentAttendance.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setItems(List<Child> items) {
        this.presentAttendance.clear();
        this.presentAttendance.addAll(items);
        this.notifyDataSetChanged();
    }

    public void removeItem(Child child) {
        this.presentAttendance.remove(child);
        this.notifyDataSetChanged();
    }

    private void changeButtonLayout(Button button) {
        button.setText(this.context.getString(R.string.pickedUpChildButtonText));
        button.setTextColor(Color.rgb(0,150,136));
        button.setBackgroundColor(Color.rgb(250, 250, 250));
        button.setEnabled(false);
    }

    interface OnSendHomeClickListener {
        void onSendHomeTapped(Child child);
        void onButtonTapped(Button button);
    }
}
