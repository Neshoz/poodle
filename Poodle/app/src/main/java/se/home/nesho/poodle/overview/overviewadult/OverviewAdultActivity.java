package se.home.nesho.poodle.overview.overviewadult;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.util.AppUtils;

/**
 * Created by Rr on 2017-04-01.
 * Adult overview.
 */

public class OverviewAdultActivity extends Activity implements OverviewAdultMvp.OverviewAdultView {

    public static final Intent newIntent(Context caller, String adultSSN) {
        Intent intent = new Intent(caller, OverviewAdultActivity.class);
        intent.putExtra(EXTRA_ADULT_SSN, adultSSN);
        return intent;
    }

    private static final String EXTRA_ADULT_SSN = "ssn";
    private OverviewAdultMvp.OverviewAdultPresenter presenter;
    private ProgressDialog progressDialog;
    private FloatingActionButton editBtn;
    private FloatingActionButton saveBtn;
    private FloatingActionButton removeBtn;
    private TextInputEditText SSNField;
    private TextInputEditText firstNameField;
    private TextInputEditText lastNameField;
    private TextInputEditText phoneNumberField;
    private TextInputEditText workNumberField;
    private TextInputEditText emailField;
    private TextView nameTitleText;
    private AppCompatSpinner relationSpinner;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview_parent);
        this.presenter = new OverviewAdultImpl(this);

        //XML
        this.SSNField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_ssn);
        this.firstNameField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_firstName);
        this.lastNameField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_lastName);
        this.phoneNumberField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_phoneNumber);
        this.workNumberField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_workNumber);
        this.nameTitleText = (TextView)findViewById(R.id.fragment_overview_parent_title);
        this.emailField = (TextInputEditText)findViewById(R.id.fragment_overview_parent_email);
        this.nameTitleText = (TextView)findViewById(R.id.fragment_overview_parent_title);
        this.relationSpinner = (AppCompatSpinner)findViewById(R.id.fragment_overview_parent_spinner);

        this.removeBtn = (FloatingActionButton)findViewById(R.id.fragment_overview_parent_remove);
        this.saveBtn = (FloatingActionButton)findViewById(R.id.fragment_overview_parent_save);
        this.editBtn = (FloatingActionButton)findViewById(R.id.fragment_overview_parent_edit);

        //OnClickListeners
        this.removeBtn.setOnClickListener( btnView -> this.presenter.onRemoveButtonTapped());
        this.saveBtn.setOnClickListener( btnView -> this.presenter.onSaveButtonTapped());
        this.editBtn.setOnClickListener( btnView -> this.presenter.onEditButtonTapped());

        // Relation spinner, adapter.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.relationship,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.getRelationSpinner().setAdapter(adapter);

        this.presenter.onLoadAdultInformation(getIntent().getStringExtra(EXTRA_ADULT_SSN));

    }

    @Override
    public TextInputEditText getSSNField() {
        return this.SSNField;
    }

    @Override
    public TextInputEditText getFirstNameField() {
        return this.firstNameField;
    }

    @Override
    public TextInputEditText getLastNameField() {
        return this.lastNameField;
    }

    @Override
    public TextInputEditText getPhoneNumberField() {
        return this.phoneNumberField;
    }

    @Override
    public TextInputEditText getWorkNumberField() {
        return this.workNumberField;
    }

    @Override
    public TextInputEditText getEmailField() {
        return this.emailField;
    }

    @Override
    public TextView getNameTitle() {
        return  this.nameTitleText;
    }

    @Override
    public AppCompatSpinner getRelationSpinner() {
        return this.relationSpinner;
    }

    @Override
    public FloatingActionButton getRemoveButton() {
        return this.removeBtn;
    }

    @Override
    public FloatingActionButton getSaveButton() {
        return this.saveBtn;
    }

    @Override
    public FloatingActionButton getEditButton() {
        return this.editBtn;
    }

    /**
     * Load current adult and fill in the fields
     * with information.
     * */
    @Override
    public void fillInFieldsWithInformation(Adult adult) {
        this.getNameTitle().setText(adult.getFirstName() + " " + adult.getLastName());
        this.getSSNField().setText(String.valueOf(adult.getSocialSecurityNumber()));
        this.getFirstNameField().setText(adult.getFirstName());
        this.getLastNameField().setText(adult.getLastName());
        this.getPhoneNumberField().setText(String.valueOf(adult.getPhoneNumber()));
        this.getWorkNumberField().setText(String.valueOf(adult.getWorkPhoneNumber()));
        this.getEmailField().setText(adult.getEmail());

        //fill in spinner with default value
        this.getRelationSpinner().setSelection(AppUtils.getRelationType().get(adult.getRole()));
    }


    /**
     * Hide all the error messages.
     * */
    @Override
    public void clearAllErrorMessages() {
        this.firstNameField.setError(null);
        this.lastNameField.setError(null);
        this.SSNField.setError(null);
        this.emailField.setError(null);
        this.phoneNumberField.setError(null);
        this.workNumberField.setError(null);
    }

    /***
     * First name field is empty error message.
     * */
    @Override
    public void showFirstNameError() {
        this.firstNameField.setError(getString(R.string.overviewEmptyFirstname));
        this.firstNameField.requestFocus();
    }

    /**
     * Last name field is empty error message.
     * */
    @Override
    public void showLastNameError() {
        this.lastNameField.setError(getString(R.string.overviewEmptyLastname));
        this.lastNameField.requestFocus();
    }

    /**
     * Phone number field is empty error message.
     * */
    @Override
    public void showPhoneNumberError() {
        this.phoneNumberField.setError(getString(R.string.overviewEmptyPhoneNumber));
        this.phoneNumberField.requestFocus();
    }

    /**
     * Work number field is empty error message.
     * */
    @Override
    public void showWorkNumberError() {
        this.workNumberField.setError(getString(R.string.overviewEmptyWorkNumber));
        this.workNumberField.requestFocus();
    }

    /**
     * When email field is empty error message.
     * */
    @Override
    public void showEmailError() {
        this.emailField.setError(getString(R.string.overviewEmptyEmail));
        this.emailField.requestFocus();
    }

    /**
     * Saving new changes fail , show error message and
     * retry alternative.
     * */
    @Override
    public void showFailToSaveError() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.fragment_overview_parent_container), this.getString(R.string.overviewFailUpdateChanges), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onSaveButtonTapped());
        snackbar.show();
    }

    @Override
    public void showFailToRemoveAdult() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.fragment_overview_parent_container), this.getString(R.string.overviewFailRemoveAdult), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.removeAdult());
        snackbar.show();
    }

    /**
     * Progress bar when updating new changes for adult.
     * */
    @Override
    public void showUpdatingInformationProgress() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.overviewDialog_title_saving),"", true);
    }

    @Override
    public void showRemovingAdultProgress() {
        this.progressDialog = ProgressDialog.show(this,this.getString(R.string.overview_Dialog_title_removing),"",true);
    }

    /**
     * Hide all progress bars.
     * */
    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    /**
     * Progress dialog when reading current adult information.
     * */
    @Override
    public void showReadingInformationProgress() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.overviewDialog_title_reading),"", true);
    }

    /**
     * Reading current adult fail, show error message.
     * */
    @Override
    public void failToReadAdultError() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.fragment_overview_parent_container), this.getString(R.string.overviewFailReadAdult), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onLoadAdultInformation(EXTRA_ADULT_SSN));
        snackbar.show();
    }

    /**
     * Remove current selected adult dialog.
     * Dialog to confirm or cancel.
     * */
    @Override
    public AlertDialog showRemoveDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(this.getString(R.string.overview_Dialog_title_remove))
                .setPositiveButton(this.getString(R.string.overview_Dialog_alternative_confirm), (dialog, which) -> this.presenter.removeAdult())
                .setNegativeButton(this.getString(R.string.overview_Dialog_alternative_cancel), (dialog, which) -> dialog.dismiss())
                .show();
    }

    /**
     * When back pressed while in edit mode, show
     * dialog if user is sure they want to go back.
     * */
    @Override
    public AlertDialog showOnBackPressedDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(this.getString(R.string.overview_Dialog_title_discard))
                .setPositiveButton(this.getString(R.string.overview_Dialog_alternative_confirm), (dialog, which) -> this.navigateToChildOverview())
                .setNegativeButton(this.getString(R.string.overview_Dialog_alternative_cancel), (dialog, which) -> dialog.dismiss())
                .show();
    }

    /**
     * Navigate to Child overview.
     * */
    @Override
    public void navigateToChildOverview() {
        //this.startActivity(new Intent(this, OverviewChildActivity.class));
        super.onBackPressed();
    }

    /**
     * When user clicks back button.
     * */
    @Override
    public void onBackPressed() {
        this.presenter.onBackPressedPressed();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }


}
