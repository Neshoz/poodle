package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Teacher;

/**
 * Created by Nesho on 2017-01-23.
 */

public interface TeacherService {

    @GET("teachers")
    Call<List<Teacher>> getAllTeachers();

    @GET("teachers/{socialSecurityNumber}")
    Call<Teacher> getTeacher(@Path("socialSecurityNumber") long socialSecurityNumber);

    @POST("teachers/post")
    Call<Teacher> postTeacher(@Body Teacher teacher);

    @DELETE("teachers/remove")
    Call<Teacher> deleteTeacher(@Body Teacher teacher);

    @POST("teachers/edit")
    Call<Teacher> editTeacher(@Body Teacher teacher);

}
