package se.home.nesho.poodle.model;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nesho on 2017-01-23.
 */

public class Child extends RealmObject {
    @PrimaryKey
    @Index
    private String socialSecurityNumber;

    @Index
    private long sectionId;

    private long childId;
    private String firstName;
    private String lastName;
    private RealmList<Adult> parents;
    private String arrival;
    private String clockOut;

    public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName, String arrival, String clockOut) {
        this.childId = childId;
        this.sectionId = sectionId;
        this.socialSecurityNumber = socialSecurityNumber;
        this.parents = new RealmList<Adult>();
        this.firstName = firstName;
        this.lastName = lastName;
        this.arrival = arrival;
        this.clockOut = clockOut;
    }
    public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName, String arrival) {
        this.childId = childId;
        this.sectionId = sectionId;
        this.socialSecurityNumber = socialSecurityNumber;
        this.parents = new RealmList<Adult>();
        this.firstName = firstName;
        this.lastName = lastName;
        this.arrival = arrival;
    }
    public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName) {
        this.childId = childId;
        this.sectionId = sectionId;
        this.socialSecurityNumber = socialSecurityNumber;
        this.parents = new RealmList<Adult>();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Child(String socialSecurityNumber, String firstName, String lastName, long sectionId){
        this.sectionId = sectionId;
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.parents = new RealmList<>();
    }
    public Child(String socialSecurityNumber, String firstName, String lastName) {
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public Child(){}

    public long getChildId() {
        return childId;
    }
    public void setChildId(long childId) {
        this.childId = childId;
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public RealmList<Adult> getParents() {
        return parents;
    }
    public void setParents(RealmList<Adult> parents) {
        this.parents = parents;
    }
    public long getSectionId() {
        return sectionId;
    }
    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }
    public String getArrival() {
        return arrival;
    }
    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
    public String getClockOut() {
        return clockOut;
    }
    public void setClockOut(String clockOut) {
        this.clockOut = clockOut;
    }
    public boolean isPresent() {
        return clockOut != null;
    }
}
