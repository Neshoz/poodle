package se.home.nesho.poodle.overview.overviewchild;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.newAdult.NewAdultActivity;
import se.home.nesho.poodle.overview.OverviewActivity;
import se.home.nesho.poodle.overview.overviewadult.OverviewAdultActivity;

/**
 * Created by Rr on 2017-02-20.
 */

public class OverviewChildActivity  extends Activity implements OverviewChildMvp.OverviewChildView, OverviewChildAdapter.OnAdultItemTappedListener {

    public static final Intent newIntent(Context caller, String ssn) {
        Intent intent = new Intent(caller, OverviewChildActivity.class);
        intent.putExtra(EXTRA_STRING_SSN, ssn);
        return intent;
    }

    public static final String EXTRA_STRING_SSN = "ssn";
    private OverviewChildAdapter adapter;
    private ProgressBar progressBar;
    private OverviewChildMvp.OverviewChildPresenter presenter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ProgressDialog progressDialog;

    private TextView titleName;
    private FloatingActionButton saveBtn;
    private FloatingActionButton addNewAdultBtn;
    private FloatingActionButton editBtn;
    private TextInputEditText childSSNField;
    private TextInputEditText childFirstNameFld;
    private TextInputEditText childLastNameFld;
    private EditText childMessageFld;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_overview);
        this.presenter = new OverviewChildPresenterImpl(this);

        //XML
        this.titleName = (TextView)findViewById(R.id.activity_child_overview_name);
        this.childSSNField = (TextInputEditText)findViewById(R.id.activity_child_overview_ssn);
        this.childFirstNameFld = (TextInputEditText)findViewById(R.id.activity_child_overview_firstName);
        this.childLastNameFld = (TextInputEditText)findViewById(R.id.activity_child_overview_lastName);
        this.childMessageFld = (EditText) findViewById(R.id.activity_child_overview_messageField);
        this.saveBtn = (FloatingActionButton) findViewById(R.id.activity_child_overview_saveBtn);
        this.addNewAdultBtn = (FloatingActionButton) findViewById(R.id.activity_child_overview_addAdultBtn);
        this.editBtn = (FloatingActionButton) findViewById(R.id.activity_child_overview_editBtn);
        this.saveBtn.setOnClickListener(view -> presenter.onSaveTapped());
        this.addNewAdultBtn.setOnClickListener(view -> this.presenter.onAddNewAdultTapped());
        this.editBtn.setOnClickListener(view -> this.presenter.onEditTapped());

        this.recyclerView = (RecyclerView)findViewById(R.id.activity_child_overview_list);
        this.adapter = new OverviewChildAdapter(this);
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(adapter);

        //Start GUI
        this.childSSNField.setEnabled(false);
        this.childFirstNameFld.setEnabled(false);
        this.childLastNameFld.setEnabled(false);
        this.childMessageFld.setEnabled(false);
        this.saveBtn.setVisibility(View.GONE);
        this.presenter.loadChildInformation(getIntent().getStringExtra(EXTRA_STRING_SSN));

    }

    @Override
    public FloatingActionButton getEditBtn() {
        return this.editBtn;
    }

    @Override
    public FloatingActionButton getSaveBtn() {
        return this.saveBtn;
    }

    @Override
    public EditText getChildMessageField() {
        return this.childMessageFld;
    }

    @Override
    public TextView getTitleName() {
        return this.titleName;
    }

    @Override
    public TextInputEditText getChildSSNField() {
        return this.childSSNField;
    }

    @Override
    public TextInputEditText getChildFirstNameField() {
        return this.childFirstNameFld;
    }

    @Override
    public TextInputEditText getChildLastNameField() {
        return this.childLastNameFld;
    }

    /**
     * Update GUI with current child information.
     * */
    @Override
    public void fillFieldsWithChildInformation(Child child) {
        this.getTitleName().setText(child.getFirstName() + " " + child.getLastName());
        this.getChildSSNField().setText(child.getSocialSecurityNumber());
        this.getChildFirstNameField().setText(child.getFirstName());
        this.getChildLastNameField().setText(child.getLastName());
    }

    @Override
    public void setAdults(List<Adult> data) {
        this.adapter.setItems(data);
    }

    /**
     * Clear all error messages.
     * */
    @Override
    public void clearAllErrorMessages() {
        this.getChildFirstNameField().setError(null);
        this.getChildLastNameField().setError(null);
    }

    /**
     * First name is empty.
     * */
    @Override
    public void showFirstNameError() {
        this.childFirstNameFld.setError(getString(R.string.overviewEmptyFirstname));
        this.childFirstNameFld.requestFocus();
    }

    /**
     * Last name field is empty.
     * */
    @Override
    public void showLastNameError() {
        this.childLastNameFld.setError(getString(R.string.overviewEmptyLastname));
        this.childLastNameFld.requestFocus();
    }

    /**
     * Failed to read current child information.
     * Give user chance to retry.
     * */
    @Override
    public void showFailToReadChildInformation() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_child_overview_container), this.getString(R.string.overviewFailReadAdult), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.loadChildInformation(getIntent().getStringExtra(EXTRA_STRING_SSN)));
        snackbar.show();
    }

    /**
     * Failed to save changes snack bar.
     * Give user chance to retry.
     * */
    @Override
    public void showFailToSaveChildInformation() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_child_overview_container), this.getString(R.string.overviewFailUpdateChanges), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.onSaveTapped());
        snackbar.show();
    }

    /**
     * Failed to read child message. Show snack bar
     * give user chance to retry.
     * */
    @Override
    public void showFailToReadChildMessage() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_child_overview_container), getString(R.string.overview_error_failToReadChildMessage), Snackbar.LENGTH_INDEFINITE)
        .setAction(this.getString(R.string.retryConnection),view -> this.presenter.onSaveTapped());
        snackbar.show();
    }

    /**
     * Progress dialog, when reading current
     * child information.
     * */
    @Override
    public void showReadingCurrentChildInformation() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.overviewDialog_title_reading),"", true);
    }

    /**
     * Progress dialog, when saving changes.
     * */
    @Override
    public void showSavingChanges() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.overviewDialog_title_saving),"", true);
    }

    /**
     * Hide all Progress bars.
     * */
    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    /**
     * Navigate to Create new Adult.
     * */
    @Override
    public void navigateToCreateAdult() {
        this.startActivity(NewAdultActivity.newIntent(this,getIntent().getStringExtra(EXTRA_STRING_SSN)));
    }

    /**
     * Go back to Overview.
     * */
    @Override
    public void navigateToOverview() {
        this.startActivity(new Intent(this, OverviewActivity.class));
    }

    /**
     * When an adult is selected from the list.
     * */
    @Override
    public void onAdultTapped(Adult adult) {
        this.startActivity(OverviewAdultActivity.newIntent(this, adult.getSocialSecurityNumber()));
    }

    /**
     * On Back press dialog.
     * */
    @Override
    public AlertDialog showOnBackPressDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(this.getString(R.string.overview_Dialog_title_discard))
                .setPositiveButton(this.getString(R.string.overview_Dialog_alternative_confirm), (dialog, which) -> this.navigateToOverview())
                .setNegativeButton(this.getString(R.string.overview_Dialog_alternative_cancel), (dialog, which) -> dialog.dismiss())
                .show();
    }


    /**
     * When user clicks back button.
     * */
    @Override
    public void onBackPressed() {
        this.presenter.onBackPressTapped();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
