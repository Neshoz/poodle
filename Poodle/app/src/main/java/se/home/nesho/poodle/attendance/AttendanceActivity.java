package se.home.nesho.poodle.attendance;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import se.home.nesho.poodle.R;

public class AttendanceActivity extends AppCompatActivity implements AttendanceMvp.AttendanceView {

    private AttendanceMvp.AttendancePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        AttendancePagerAdapter adapter = new AttendancePagerAdapter(this.getSupportFragmentManager(), this);
        ViewPager viewPager = (ViewPager)findViewById(R.id.content_container);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        presenter = new AttendancePresenterImpl(this);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        this.presenter.onDestroy();
        super.onBackPressed();
    }
}
