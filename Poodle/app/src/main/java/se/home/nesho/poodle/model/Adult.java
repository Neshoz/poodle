package se.home.nesho.poodle.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class Adult extends RealmObject {
    @Index
    @PrimaryKey
    private String socialSecurityNumber;

    @Index
    private String role;

    private long adultId;
    private String phoneNumber;
    private String workPhoneNumber;
    private String firstName;
    private String lastName;
    private String email;


    public Adult(String firstName, String lastName, String socialSecurityNumber, String phoneNumber, String workPhoneNumber, String email, String role){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.phoneNumber = phoneNumber;
        this.workPhoneNumber = workPhoneNumber;
        this.email = email;
        this.role = role;
    }
    public Adult(){}

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public long getAdultId() {
        return adultId;
    }

    public void setAdultId(long adultId) {
        this.adultId = adultId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String toString() {
        return this.firstName + " " + this.lastName + " - " + this.socialSecurityNumber;
    }
}
