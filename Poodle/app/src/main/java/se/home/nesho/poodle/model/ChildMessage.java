package se.home.nesho.poodle.model;

/**
 * Created by Nesho on 2017-04-24.
 */

public class ChildMessage {
    private Child child;
    private String message;

    public ChildMessage(Child child, String message) {
        this.child = child;
        this.message = message;
    }
    public ChildMessage(String message) {
        this.message = message;
    }
    public ChildMessage() {

    }

    public Child getChild() {
        return child;
    }
    public void setChild(Child child) {
        this.child = child;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
