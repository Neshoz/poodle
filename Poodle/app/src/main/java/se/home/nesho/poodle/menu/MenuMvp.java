package se.home.nesho.poodle.menu;

/**
 * Created by Nesho on 2017-02-14.
 */
public interface MenuMvp {

    interface MenuView {
        void navigateToAttendance();
        void navigateToOverview();
        void navigateToLogin();
        void navigateToEmergency();
    }

    interface MenuPresenter {
        void onAttendanceTapped();
        void onOverviewTapped();
        void onLogoutTapped();
        void onEmergencyTapped();
        void onDestroy();
    }

}
