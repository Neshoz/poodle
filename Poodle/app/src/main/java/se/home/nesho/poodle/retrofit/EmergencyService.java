package se.home.nesho.poodle.retrofit;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Emergency;

/**
 * Created by Nesho on 2017-01-23.
 */

public interface EmergencyService {

    @GET("emergencies")
    Call<List<Emergency>> getAllEmergencies();

    @GET("emergencies/{date}")
    Call<Emergency> getEmergency(@Path("date") Date date);

    @POST("emergencies")
    Call<Emergency> postEmergency(@Body Emergency emergency);
}
