package se.home.nesho.poodle.attendance.summary;

import java.util.List;

import se.home.nesho.poodle.model.AttendanceOverview;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.retrofit.AttendanceRestController;
import se.home.nesho.poodle.util.AppUtils;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-22.
 */

public class SummaryPresenterImpl implements SummaryMvp.Presenter {

    private SummaryMvp.View view;
    private AttendanceRestController restController;

    public SummaryPresenterImpl(SummaryMvp.View view) {
        this.view = view;
        this.restController = new AttendanceRestController();
    }

    @Override
    public void loadSummary() {
        this.view.showProgress();
        this.restController.getOverview(AppUtils.getCurrentDate(), Constants.CURRENT_SECTION, new AttendanceRestController.OnOverviewLoadedCallback() {
            @Override
            public void onOverviewLoaded(List<Child> overview) {
                view.hideProgress();
                view.setSummary(overview);
            }

            @Override
            public void onOverviewLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void onErrorMessageRetryTapped() {
        this.loadSummary();
    }

    @Override
    public void checkOutChild(Child child) {
        this.view.showClockOutProgress();
        this.restController.clockOut(child.getSocialSecurityNumber(), AppUtils.getCurrentDate(), new ClockOut(AppUtils.getCurrentTime()), new AttendanceRestController.OnClockOutCallback() {
            @Override
            public void onClockOutSuccess() {
                view.hideProgress();
                view.changeButtonLayout();
            }

            @Override
            public void onClockOutFailure(Throwable t) {
                view.hideProgress();
                view.showClockOutFailed();
            }
        });
    }

    @Override
    public void onChildTapped(Child child) {
        this.view.navigateToCheckIn(child);
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
