package se.home.nesho.poodle.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import se.home.nesho.poodle.model.Adult;

/**
 * Created by Nesho & 3xR on 2017-01-23.
 */

public interface AdultService {

    @GET("adults")
    Call<List<Adult>> getAllAdults();

    @GET("adults/{socialSecurityNumber}")
    Call<Adult> getAdult(@Path("socialSecurityNumber") String socialSecurityNumber);

    @POST("adults/{childSSN}")
    Call<Adult> postAdult(@Path("childSSN") String childSSN, @Body Adult adult);

    @POST("adults/{socialSecurityNumber}/updateAdult")
    Call<Adult> editAdult(@Path("socialSecurityNumber") String socialSecurityNumber, @Body Adult adult);

    @POST("adults/remove")
    Call<Adult> removeAdult(@Body Adult adult);

}
