package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.AttendanceOverview;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.model.Present;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class AttendanceRestController {

    public void getAllAttendance(OnGetAllAttendanceCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<List<Attendance>> call = service.getAllAttendances();

        call.enqueue(new Callback<List<Attendance>>() {
            @Override
            public void onResponse(Call<List<Attendance>> call, Response<List<Attendance>> response) {
                List<Attendance> attendance = response.body();
                callback.onGetAllAttendanceLoaded(attendance);
            }

            @Override
            public void onFailure(Call<List<Attendance>> call, Throwable t) {
                callback.onGetAllAttendanceLoadedFailure(t);
                Log.d("Retrofit Failure :", "Failure fetching all attendance - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getAllAttendanceForChild (long socialSecurityNumber, OnGetAttendanceForChildCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<List<Attendance>> call = service.getAttendanceForChild(socialSecurityNumber);

        call.enqueue(new Callback<List<Attendance>>() {
            @Override
            public void onResponse(Call<List<Attendance>> call, Response<List<Attendance>> response) {
                List<Attendance> attendance = response.body();
                callback.onGetAttendanceForChildLoaded(attendance);
            }

            @Override
            public void onFailure(Call<List<Attendance>> call, Throwable t) {
                callback.onGetAttendanceForChildLoadedFailure(t);
                Log.d("Retrofit Failure :", "Failure fetching all attendance from child with: -" + socialSecurityNumber + " Error: " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getOverview(String date, String section, OnOverviewLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<List<Child>> call = service.getOverview(date, section);

        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                List<Child> overview = response.body();
                Log.d("Retrofit", "Summary loaded - " + response.body().size());
                callback.onOverviewLoaded(overview);
            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                callback.onOverviewLoadedFailure(t);
                Log.d("Retrofit Failure :", "Failure fetching attendance overview - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getDueToArrive(String section, String date, OnDueToArriveLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<List<Child>> call = service.getDueToArrive(section, date);

        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                List<Child> data = response.body();
                callback.onDueToArriveLoaded(data);
                Log.d("Retrofit Success", "Success fetching due to arrive data - " + response.code());
            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                callback.onDueToArriveLoadedFailure(t);
                Log.d("Retrofit Failure", "Failure fetching due to arrive data - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getPresentChildren(String section, String date, OnPresentChildrenLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<List<Child>> call = service.getPresentChildren(section, date);

        call.enqueue(new Callback<List<Child>>() {
            @Override
            public void onResponse(Call<List<Child>> call, Response<List<Child>> response) {
                List<Child> data = response.body();
                callback.onPresentChildrenLoaded(data);
                Log.d("Retrofit", "Success fetching present children - " + response.code());
            }

            @Override
            public void onFailure(Call<List<Child>> call, Throwable t) {
                t.printStackTrace();
                callback.onPresentChildrenLoadedFailure(t);
            }
        });
    }

    public void postAttendance (Attendance attendance, OnPostAttendanceCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<Attendance> call = service.postAttendance(attendance);

        call.enqueue(new Callback<Attendance>() {
            @Override
            public void onResponse(Call<Attendance> call, Response<Attendance> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onAttendancePosted();
                    Log.d("Retrofit Sucess","new Attendance posted");
                } else {
                    Log.d("Retrofit","Something went wrong on posting, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Attendance> call, Throwable t) {
                callback.onAttendancePostedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new Attendance," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void clockOut(String ssn, String date, ClockOut clockOut, OnClockOutCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<ClockOut> call = service.clockOut(ssn, date, clockOut);

        call.enqueue(new Callback<ClockOut>() {
            @Override
            public void onResponse(Call<ClockOut> call, Response<ClockOut> response) {
                Log.d("Retrofit", "Success clocking out - " + response.code());
                if(call.isExecuted() && response.code() == 204) {
                    callback.onClockOutSuccess();
                }
            }

            @Override
            public void onFailure(Call<ClockOut> call, Throwable t) {
                Log.d("Retrofit", "Failure clocking out");
                callback.onClockOutFailure(t);
                t.printStackTrace();
            }
        });
    }

    public void clockIn(Present attendance, OnClockInCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<Present> call = service.clockIn(attendance);

        call.enqueue(new Callback<Present>() {
            @Override
            public void onResponse(Call<Present> call, Response<Present> response) {
                Log.d("Retrofit", "Success clocking in - " + response.code());
                if(call.isExecuted() && response.code() == 204) {
                    callback.onClockInSuccess();
                }
            }

            @Override
            public void onFailure(Call<Present> call, Throwable t) {
                Log.d("Retrofit", "Failure clocking in");
                callback.onClockInFailure(t);
                t.printStackTrace();
            }
        });
    }

    public void postOrUpdateMessage(ChildMessage message, OnMessagePostedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<ChildMessage> call = service.postOrUpdateMessage(message);

        call.enqueue(new Callback<ChildMessage>() {
            @Override
            public void onResponse(Call<ChildMessage> call, Response<ChildMessage> response) {
                Log.d("Retrofit", "Code - " + response.code());
                if(call.isExecuted() && response.code() == 204) {
                    Log.d("Retrofit", "Success posting message");
                    callback.onPostSuccess();
                }
            }

            @Override
            public void onFailure(Call<ChildMessage> call, Throwable t) {
                Log.d("Retrofit", "Failure posting message");
                t.printStackTrace();
                callback.onPostFailure();
            }
        });
    }

    public void getMessage(String ssn, OnMessageLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AttendanceService service = retrofit.create(AttendanceService.class);
        Call<ChildMessage> call = service.getMessage(ssn);

        call.enqueue(new Callback<ChildMessage>() {
            @Override
            public void onResponse(Call<ChildMessage> call, Response<ChildMessage> response) {
                ChildMessage message = response.body();
                Log.d("Retrofit", "Success fetching message - " + response.code());
                callback.onMessageLoaded(message);
            }

            @Override
            public void onFailure(Call<ChildMessage> call, Throwable t) {
                Log.d("Retrofit", "Failure fetching message");
                t.printStackTrace();
                callback.onMessageLoadedFailure();
            }
        });
    }

    public interface OnMessageLoadedCallback {
        void onMessageLoaded(ChildMessage message);
        void onMessageLoadedFailure();
    }

    public interface OnGetAllAttendanceCallback {
        void onGetAllAttendanceLoaded(List<Attendance> data);
        void onGetAllAttendanceLoadedFailure(Throwable t);
    }

    public interface  OnGetAttendanceForChildCallback {
        void onGetAttendanceForChildLoaded(List<Attendance> data);
        void onGetAttendanceForChildLoadedFailure(Throwable t);
    }

    public interface OnPostAttendanceCallback {
        void onAttendancePosted();
        void onAttendancePostedFailure(Throwable t);
    }

    public interface OnClockOutCallback {
        void onClockOutSuccess();
        void onClockOutFailure(Throwable t);
    }

    public interface OnClockInCallback {
        void onClockInSuccess();
        void onClockInFailure(Throwable t);
    }

    public interface OnOverviewLoadedCallback {
        void onOverviewLoaded(List<Child> overview);
        void onOverviewLoadedFailure(Throwable t);
    }

    public interface OnDueToArriveLoadedCallback {
        void onDueToArriveLoaded(List<Child> data);
        void onDueToArriveLoadedFailure(Throwable t);
    }

    public interface OnPresentChildrenLoadedCallback {
        void onPresentChildrenLoaded(List<Child> data);
        void onPresentChildrenLoadedFailure(Throwable t);
    }

    public interface OnMessagePostedCallback {
        void onPostSuccess();
        void onPostFailure();
    }

    //TODO  ClockOut
}
