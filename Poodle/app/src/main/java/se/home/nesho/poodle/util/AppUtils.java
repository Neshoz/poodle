package se.home.nesho.poodle.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Nesho & Rr on 2017-01-24.
 */

public class AppUtils {

    public static String getCurrentDate() {
        Date date = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return dateFormatter.format(date);
    }

    public static String getCurrentTime() {
        Date date = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        return dateFormatter.format(date);
    }

    /**
     * Get current relation type.
     * Converts String to integer for spinners.
     * */
    public static Map<String, Integer> getRelationType() {
        Map<String, Integer> relationType;
        relationType = new HashMap<>();
        relationType.put("Mother",0);
        relationType.put("Father",1);
        relationType.put("StepParent",2);
        relationType.put("Family",3);
        relationType.put("Relative",4);
        relationType.put("Friend",5);
        relationType.put("NotAllowed",6);
        return relationType;
    }

    /**
     * Map converter, supports swapping input/output from maps.
     * */
    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

}
