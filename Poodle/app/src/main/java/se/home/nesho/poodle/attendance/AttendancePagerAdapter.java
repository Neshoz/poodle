package se.home.nesho.poodle.attendance;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.attendance.absent.AbsentFragment;
import se.home.nesho.poodle.attendance.present.PresentFragment;
import se.home.nesho.poodle.attendance.summary.SummaryFragment;

/**
 * Created by Nesho on 2017-02-17.
 */

public class AttendancePagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public AttendancePagerAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PresentFragment();
            case 1:
                return new AbsentFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return this.context.getString(R.string.presentTab);
            case 1:
                return this.context.getString(R.string.due_to_arrive);
        }
        return null;
    }
}
