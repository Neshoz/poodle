package se.home.nesho.poodle.checkin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.attendance.AttendanceActivity;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;

public class CheckinActivity extends Activity implements CheckinMvp.CheckinView {

    private static final String EXTRA_STRING_SSN = "ssn";
    private CheckinMvp.CheckinPresenter presenter;
    private TextView childNameTextView;
    private TextView childSSNTextView;
    private TextInputEditText childMessageInput;
    private TextInputLayout childMessageInputLayout;
    private LinearLayout pickUpRowLayout;
    private LinearLayout containerLayout;
    private FloatingActionButton confirmButton;
    private AppCompatSpinner spinner;
    private ProgressDialog progressDialog;
    private ArrayAdapter<Adult> adapter;
    private Child child;

    public static final Intent newIntent(Context caller, String ssn) {
        Intent intent = new Intent(caller, CheckinActivity.class);
        intent.putExtra(EXTRA_STRING_SSN, ssn);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);

        childNameTextView = (TextView)findViewById(R.id.child_name_textview);
        childSSNTextView = (TextView)findViewById(R.id.child_ssn_textview);
        pickUpRowLayout = (LinearLayout)findViewById(R.id.pick_up_row_layout);
        containerLayout = (LinearLayout)findViewById(R.id.container_layout);
        confirmButton = (FloatingActionButton)findViewById(R.id.publish_attendance_button);
        childMessageInput = (TextInputEditText)findViewById(R.id.teacher_message_input);
        childMessageInputLayout = (TextInputLayout)findViewById(R.id.message_input_layout);
        spinner = (AppCompatSpinner)findViewById(R.id.picked_up_by_spinner);
        SwitchCompat switchCompat = (SwitchCompat)findViewById(R.id.attendance_switch);

        this.adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        this.adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        presenter = new CheckinPresenterImpl(this);
        presenter.loadPresentChild();
        presenter.loadMessage();
        presenter.loadPickUpAdults();

        confirmButton.setOnClickListener(view -> this.presenter.clockInChild());

        switchCompat.setOnCheckedChangeListener((switchButton, activated) -> this.presenter.updateUI(activated));
    }

    @Override
    public void setChild(Child child) {
        this.child = child;
        this.childNameTextView.setText(child.getFirstName() + " " + child.getLastName());
        this.childSSNTextView.setText(String.valueOf(child.getSocialSecurityNumber()));
    }

    @Override
    public void showClockInProgress() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.postActionDialogTitle), "", false);
    }

    @Override
    public void hideClockInProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showGetChildProgress() {
        this.progressDialog = ProgressDialog.show(this, this.getString(R.string.progressDialogTitle), this.getString(R.string.fetchDTAChildProgressDialogMessage), false);
    }

    @Override
    public void hideGetChildProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.onConnectionFailedTapped());
        snackbar.show();
    }

    @Override
    public void showClockInFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.onClockInFailedTapped());
        snackbar.show();
    }

    @Override
    public void setMessage(ChildMessage message) {
        this.childMessageInput.setText(message.getMessage());
    }

    @Override
    public void setPickUpAdults(List<Adult> adults) {
        this.adapter.clear();
        this.adapter.addAll(adults);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void showAdultsLoadedFailed() {
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_checkin), this.getString(R.string.downloadAdultsFailure), Snackbar.LENGTH_LONG)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.loadPickUpAdults());
        snackbar.show();
    }

    @Override
    public void setAdultPickUpError() {
        Toast.makeText(this, getString(R.string.pickUpAdultError), Toast.LENGTH_LONG).show();
    }

    @Override
    public Adult getSelectedAdult() {
        return (Adult)this.spinner.getSelectedItem();
    }

    @Override
    public String getMessage() {
        return this.childMessageInput.getText().toString();
    }

    @Override
    public String getChildSSN() {
        return this.getIntent().getExtras().getString("ssn");
    }

    @Override
    public LinearLayout getPickUpRowLayout() {
        return this.pickUpRowLayout;
    }

    @Override
    public LinearLayout getContainerLayout() {
        return this.containerLayout;
    }

    @Override
    public FloatingActionButton getClockInButton() {
        return this.confirmButton;
    }

    @Override
    public Child getChild() {
        return this.child;
    }

    @Override
    public TextInputLayout getMessageInputLayout() {
        return this.childMessageInputLayout;
    }

    @Override
    public void navigateToAttendance() {
        this.startActivity(new Intent(this, AttendanceActivity.class));
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
