package se.home.nesho.poodle.retrofit;

import android.util.Log;
import android.widget.ArrayAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Section;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class SectionRestController {

    public void getAllSections(final OnSectionsLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SectionService service = retrofit.create(SectionService.class);
        Call<List<Section>> call = service.getAllSections();

        call.enqueue(new Callback<List<Section>>() {
            @Override
            public void onResponse(Call<List<Section>> call, Response<List<Section>> response) {
                List<Section> sections = response.body();
                callback.onSectionsLoaded(sections);
                for(Section section : response.body()) {
                    Log.d("Retrofit Success: ", section.getSectionName());
                }
            }

            @Override
            public void onFailure(Call<List<Section>> call, Throwable t) {
                callback.onSectionsLoadedFailure(t);
                Log.d("Retrofit Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getSection(String sectionName, final OnSectionLoadedCallBack callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SectionService service = retrofit.create(SectionService.class);
        Call<Section> call = service.getSection(sectionName);

        call.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                Section section = response.body();
                callback.onSectionLoaded(section);
                Log.d("Retrofit Success: ", section.getSectionName());
            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                Log.d("Retrofit Failure: ", t.getMessage());
                callback.onSectionLoadedFailure(t);
                t.printStackTrace();
            }
        });
    }

    /**
     * This interface is used to create callbacks, reason why we do this is so we can control and fetch the data when it's done loading. Se sample in LoginPresenterImpl
     */
    public interface OnSectionsLoadedCallback {
        void onSectionsLoaded(List<Section> sections);
        void onSectionsLoadedFailure(Throwable t);
    }

    public interface OnSectionLoadedCallBack {
        void onSectionLoaded(Section section);
        void onSectionLoadedFailure(Throwable t);
    }

}
