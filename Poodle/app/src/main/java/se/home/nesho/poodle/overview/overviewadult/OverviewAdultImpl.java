package se.home.nesho.poodle.overview.overviewadult;

import android.util.Log;
import android.view.View;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.retrofit.AdultRestController;
import se.home.nesho.poodle.util.AppUtils;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Rr on 2017-04-01.
 * Presenter for OverviewAdult.
 */

public class OverviewAdultImpl implements OverviewAdultMvp.OverviewAdultPresenter {

    private OverviewAdultMvp.OverviewAdultView view;
    private AdultRestController restController;
    private Adult selectedAdult;
    private boolean isEditMode;

    public OverviewAdultImpl(OverviewAdultMvp.OverviewAdultView view) {
        this.view = view;
        this.restController = new AdultRestController();
        this.isEditMode = false;
    }

    /**
     * Loading current adult information.
     * */
    @Override
    public void onLoadAdultInformation(String adultSSN) {
        view.clearAllErrorMessages();
        //Update gui at start.
        this.editModeGui();

        //Read current information.
        this.view.showReadingInformationProgress();
        this.restController.getAdult(adultSSN, new AdultRestController.OnAdultLoadedCallback() {
            @Override
            public void onAdultLoaded(Adult adult) {
                view.hideProgress();
                selectedAdult = adult;
                view.fillInFieldsWithInformation(selectedAdult);
            }

            @Override
            public void onAdultLoadedFailure(Throwable t) {
                view.hideProgress();
                view.failToReadAdultError();
            }
        });
    }

    /**
     * When remove button tapped.
     * */
    @Override
    public void onRemoveButtonTapped() {
        view.showRemoveDialog();
    }

    /**
     * Remove current adult.
     * */
    @Override
    public void removeAdult() {
        view.clearAllErrorMessages();
        this.view.showRemovingAdultProgress();
        this.restController.removeAdult(selectedAdult, new AdultRestController.OnRemoveAdultCallback() {
            @Override
            public void onRemoveAdultLoaded() {
                view.hideProgress();
                view.navigateToChildOverview();
            }

            @Override
            public void onRemoveAdultLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showFailToRemoveAdult();
            }
        });
    }

    /**
     * When save button tapped.
     * Check validation on all the fields.
     * When every thing looks good, update the current adult.
     * */
    @Override
    public void onSaveButtonTapped() {
        view.clearAllErrorMessages();
        this.view.clearAllErrorMessages();
        if(validateInputFields() && isEditMode) {
            selectedAdult.setFirstName(view.getFirstNameField().getText().toString());
            selectedAdult.setLastName(view.getLastNameField().getText().toString());
            selectedAdult.setPhoneNumber(view.getPhoneNumberField().getText().toString());
            selectedAdult.setWorkPhoneNumber(view.getWorkNumberField().getText().toString());
            selectedAdult.setEmail(view.getEmailField().getText().toString());
            selectedAdult.setRole(view.getRelationSpinner().getSelectedItem().toString());

            //Save new changes after validation succeeded.
            saveChanges(selectedAdult);

        }
    }

    /**
     * Save new adult information.
     * */
    private void saveChanges(Adult adult) {
        this.view.showUpdatingInformationProgress();
        this.restController.editAdult(adult, new AdultRestController.OnEditAdultCallback() {
            @Override
            public void onEditAdultLoaded() {
                view.hideProgress();
                isEditMode = false;
                editModeGui();
            }

            @Override
            public void onEditAdultLoadedFailure(Throwable t) {
                view.hideProgress();
                view.showFailToSaveError();
            }
        });
    }

    /**
     * When edit button tapped.
     * */
    @Override
    public void onEditButtonTapped() {
        view.clearAllErrorMessages();
        if(isEditMode) {
            this.isEditMode = false;
            this.editModeGui();
        } else {
            this.isEditMode = true;
            this.editModeGui();
        }
    }

    /**
     * When back pressed tapped.
     * */
    @Override
    public void onBackPressedPressed() {
        if(isEditMode) {
            this.view.showOnBackPressedDialog();
        } else {
            this.view.navigateToChildOverview();
        }
    }

    /**
     * Update gui depending on isEditMode.
     * */
    private void editModeGui() {
        if(isEditMode) {
            view.getFirstNameField().setEnabled(true);
            view.getLastNameField().setEnabled(true);
            view.getPhoneNumberField().setEnabled(true);
            view.getWorkNumberField().setEnabled(true);
            view.getRelationSpinner().setEnabled(true);
            view.getEmailField().setEnabled(true);
            view.getEditButton().setVisibility(View.GONE);
            view.getRemoveButton().setVisibility(View.VISIBLE);
            view.getSaveButton().setVisibility(View.VISIBLE);
        } else {
            view.getFirstNameField().setEnabled(false);
            view.getLastNameField().setEnabled(false);
            view.getPhoneNumberField().setEnabled(false);
            view.getWorkNumberField().setEnabled(false);
            view.getRelationSpinner().setEnabled(false);
            view.getEmailField().setEnabled(false);
            view.getEditButton().setVisibility(View.VISIBLE);
            view.getRemoveButton().setVisibility(View.GONE);
            view.getSaveButton().setVisibility(View.GONE);
        }
    }

    /**
     * Check if there is any empty fields that are
     * not allowed to be empty.
     * */
    private boolean validateInputFields() {
        if (view.getFirstNameField().getText().toString().trim().equals(Constants.EMPTY)) {
            this.view.showFirstNameError();
            return false;
        } else if (view.getLastNameField().getText().toString().trim().equals(Constants.EMPTY)) {
            this.view.showLastNameError();
            return false;
        } else if (view.getPhoneNumberField().getText().toString().trim().equals(Constants.EMPTY)) {
            this.view.showPhoneNumberError();
            return false;
        } else if (view.getWorkNumberField().getText().toString().trim().equals(Constants.EMPTY)) {
            this.view.showWorkNumberError();
            return false;
        } else if (view.getEmailField().getText().toString().trim().equals(Constants.EMPTY)) {
            this.view.showEmailError();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
