package se.home.nesho.poodle.model;

import java.util.Date;

import io.realm.annotations.Index;

/**
 * Created by Nesho on 2017-01-23.
 */

public class Emergency {
    @Index
    private Date emergencyDate;

    @Index
    private long sectionId;

    private int emergencyId;

    public Emergency(long sectionId){
        this.sectionId = sectionId;
    }
    public Emergency(){}

    public int getEmergencyId() {
        return emergencyId;
    }
    public void setEmergencyId(int emergencyId) {
        this.emergencyId = emergencyId;
    }
    public Date getEmergencyDate() {
        return emergencyDate;
    }
    public void setEmergencyDate(Date emergencyDate) {
        this.emergencyDate = emergencyDate;
    }
    public long getSectionId() {
        return sectionId;
    }
    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }
}
