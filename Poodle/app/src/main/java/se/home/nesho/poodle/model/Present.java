package se.home.nesho.poodle.model;

import io.realm.RealmObject;

/**
 * Created by Nesho on 2017-02-27.
 */

public class Present extends RealmObject {
    private long presentId;
    private Section section;
    private Child child;
    private String date;
    private String arrival;
    private boolean sentHome;

    public Present(long presentId, Section section, Child child, String date, String arrival, boolean sentHome) {
        this.presentId = presentId;
        this.section = section;
        this.child = child;
        this.date = date;
        this.arrival = arrival;
        this.sentHome = sentHome;
    }
    public Present(long presentId, Section section, Child child, String date, String arrival) {
        this.presentId = presentId;
        this.section = section;
        this.child = child;
        this.date = date;
        this.arrival = arrival;
    }
    public Present(Section section, Child child, String date, String arrival) {
        this.section = section;
        this.child = child;
        this.date = date;
        this.arrival = arrival;
    }
    public Present(Section section, Child child, String date) {
        this.section = section;
        this.child = child;
        this.date = date;
    }
    public Present(Child child, String date, String arrival) {
        this.child = child;
        this.date = date;
        this.arrival = arrival;
    }
    public Present(String arrival) {
        this.arrival = arrival;
    }
    public Present() {

    }

    public long getPresentId() {
        return presentId;
    }
    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }
    public Section getSection() {
        return section;
    }
    public void setSection(Section section) {
        this.section = section;
    }
    public Child getChild() {
        return child;
    }
    public void setChild(Child child) {
        this.child = child;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getArrival() {
        return arrival;
    }
    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
    public boolean isSentHome() {
        return sentHome;
    }
    public void setSentHome(boolean sentHome) {
        this.sentHome = sentHome;
    }
}
