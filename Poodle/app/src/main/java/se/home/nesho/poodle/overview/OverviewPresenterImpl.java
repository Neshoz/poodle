package se.home.nesho.poodle.overview;


import android.util.Log;

import java.util.List;

import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.retrofit.ChildRestController;
import se.home.nesho.poodle.util.Constants;



/**
 * Created by Rr on 2017-02-20.
 */

public class OverviewPresenterImpl implements OverviewMvp.OverviewPresenter{

    private OverviewMvp.OverviewView view;
    private ChildRestController childRestController;


    public OverviewPresenterImpl(OverviewMvp.OverviewView view) {
        this.view = view;
        this.childRestController = new ChildRestController();
    }

    /**
     * When back press is tapped.
     * */
    @Override
    public void onBackPressedTapped() {
        this.view.navigateToMenu();
    }

    @Override
    public void loadChildren() {
        this.view.showProgress();
        this.childRestController.getChildrenInSection(new ChildRestController.onChildrenInSectionLoadedCallback() {
            @Override
            public void onChildrenInSectionLoaded(List<Child> data) {
                if (data == null) {
                    view.hideProgress();
                    view.showConnectionFailed();

                } else if (data.size() > 0) {
                    view.setChildren(data);
                    view.hideProgress();

                } else if (data.size() < 0) {
                    view.errorNoChildrenFound();
                    view.hideProgress();
                }
            }

            @Override
            public void onChildrenInSectionFailure(Throwable t) {
                view.hideProgress();
                view.showConnectionFailed();
            }
        });
    }

    /**
     * Retry when failing to get all children's from retrofit.
     * */
    @Override
    public void onRetryTapped() {
        this.loadChildren();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

}


