package se.home.nesho.poodle;

import java.util.List;

import se.home.nesho.poodle.model.Section;

/**
 * Created by Nesho on 2017-01-19.
 */

public interface LoginView {
    String getSection();
    long getSSN();
    String getPassword();
    //We use this to update our view
    void setSections(List<Section> sections);
    void showProgress();
    void hideProgress();
    void showAuthenticateProgress();
    void hideAuthenticateProgress();
    void showConnectionFailed();
    void setSSNError();
    void setPasswordError();
    void disablePassword();
    void navigateToPanel();
}
