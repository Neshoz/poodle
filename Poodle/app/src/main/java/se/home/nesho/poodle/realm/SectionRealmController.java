package se.home.nesho.poodle.realm;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import io.realm.Realm;
import se.home.nesho.poodle.model.Section;

/**
 * Created by Nesho on 2017-01-24.
 */

public class SectionRealmController {

    public List<Section> getAllSections(){
        Realm realm = Realm.getDefaultInstance();
        return new LinkedList<>(realm.where(Section.class).findAll());
    }

    public Section getSection(String sectionName){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Section.class).equalTo("sectionName", sectionName).findFirst();
    }

    public void postSection(final Section section){
        try{
            final Realm realmInstance = Realm.getDefaultInstance();
            realmInstance.executeTransactionAsync((realm) -> {
                section.setSectionId(realm.where(Section.class).count() + 1);
                realm.insertOrUpdate(section);
            },  () -> {
                if(realmInstance != null){
                    realmInstance.close();
                }
            }, (error) -> {
                realmInstance.close();
                Log.d("Realm Failure", error.getMessage());
                error.printStackTrace();
            });
        }
        catch (Exception ex){
            Log.d("Realm failure", ex.getMessage());
        }
    }

    public void deleteSection(final Section section) {
        try{
            final Realm realmInstance = Realm.getDefaultInstance();
            realmInstance.executeTransactionAsync((realm) -> {
                Section realmSection = realm.where(Section.class).equalTo("sectionName", section.getSectionName()).findFirst();
                realmSection.deleteFromRealm();
            }, () -> {
                if(realmInstance != null){
                    realmInstance.close();
                }
            }, (error) -> {
                realmInstance.close();
                Log.d("Realm Failure", error.getMessage());
                error.printStackTrace();
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void deleteEverything(){
        try{
            final Realm realmInstance = Realm.getDefaultInstance();
            realmInstance.executeTransactionAsync((realm) -> {
                realm.delete(Section.class);
            }, () -> {
                if(realmInstance != null){
                    realmInstance.close();
                    Log.d("Success", "Deleted All Sections");
                }
            }, (error) -> {
                realmInstance.close();
                Log.d("Realm Failure", error.getMessage());
                error.printStackTrace();
            });
        }
        catch (Exception ex){
            Log.d("Realm Failure", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
