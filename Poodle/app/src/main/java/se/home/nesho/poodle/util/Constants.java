package se.home.nesho.poodle.util;

import se.home.nesho.poodle.model.Teacher;

/**
 * Created by Nesho on 2017-01-23.
 */

public class Constants {
    // NESHO BASE URL
    public static final String BASE_API_URL = "http://192.168.1.77:8080/TibetanMastiff/webapi/";

    // RR BASE URL
    //public static final String BASE_API_URL = "http://192.168.0.111:8080/TibetanMastiff/webapi/";

    // Newton BASE URL
    // public static final String BASE_API_URL = "http://10.11.12.217:8080/TibetanMastiff/webapi/";

    //Lighthouse BASE URL
    //public static final String BASE_API_URL = "http://192.168.30.58:8080/TibetanMastiff/webapi/";


    public static String CURRENT_SECTION = "";
    public static Teacher LOGGED_IN_TEACHER = null;
    public static final String EMPTY = "";
}
