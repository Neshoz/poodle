package se.home.nesho.poodle.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nesho on 2017-01-23.
 */

public class Section extends RealmObject {
    @PrimaryKey
    @Index
    private long sectionId;
    private String password;

    @Index
    private String sectionName;

    public Section(int sectionId, String sectionName){
        this.sectionId = sectionId;
        this.sectionName = sectionName;
    }
    public Section(String sectionName) {
        this.sectionName = sectionName;
    }
    public Section(){}

    public long getSectionId() {
        return sectionId;
    }
    public String getPassword(){
        return password;
    }
    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }
    public String getSectionName() {
        return sectionName;
    }
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    public String toString(){
        return getSectionName();
    }
}
