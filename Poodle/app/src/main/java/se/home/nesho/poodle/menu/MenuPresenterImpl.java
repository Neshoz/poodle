package se.home.nesho.poodle.menu;

/**
 * Created by Nesho on 2017-01-30.
 */

public class MenuPresenterImpl implements MenuMvp.MenuPresenter {

    private MenuMvp.MenuView view;

    public MenuPresenterImpl(MenuMvp.MenuView view) {
        this.view = view;
    }

    @Override
    public void onAttendanceTapped() {
        this.view.navigateToAttendance();
    }

    @Override
    public void onOverviewTapped() {
        this.view.navigateToOverview();
    }

    @Override
    public void onLogoutTapped() {
        this.view.navigateToLogin();
    }

    @Override
    public void onEmergencyTapped() {
        this.view.navigateToEmergency();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
