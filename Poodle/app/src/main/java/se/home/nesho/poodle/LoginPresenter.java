package se.home.nesho.poodle;

/**
 * Created by Nesho on 2017-01-19.
 */

public interface LoginPresenter {
    void validateCredentials();
    //In this method we use our SectionRestController to call getAllSections
    void loadSections();
    void onErrorMessageRetryTapped();
    void onDestroy();
}