package se.home.nesho.poodle.model;

import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Nesho on 2017-01-23.
 */

public class Admin {
    @PrimaryKey
    private long adminId;
    private String firstName;
    private String lastName;
    @Index
    private String username;
    private String password;

    public Admin(String firstName, String lastName, String username, String password){
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }
    public Admin(){}

    public long getAdminId() {
        return adminId;
    }
    public void setAdminId(long adminId) {
        this.adminId = adminId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
