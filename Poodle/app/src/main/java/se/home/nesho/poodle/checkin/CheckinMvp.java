package se.home.nesho.poodle.checkin;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.widget.LinearLayout;

import java.util.List;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;

/**
 * Created by Nesho on 2017-02-15.
 */

public interface CheckinMvp {

    interface CheckinView {
        void setChild(Child child);
        void showClockInProgress();
        void hideClockInProgress();
        void showGetChildProgress();
        void hideGetChildProgress();
        void showConnectionFailed();
        void showClockInFailed();
        void setMessage(ChildMessage message);
        void setPickUpAdults(List<Adult> adults);
        void showAdultsLoadedFailed();
        void setAdultPickUpError();
        Adult getSelectedAdult();
        String getMessage();
        String getChildSSN();
        LinearLayout getPickUpRowLayout();
        LinearLayout getContainerLayout();
        FloatingActionButton getClockInButton();
        Child getChild();
        TextInputLayout getMessageInputLayout();
        void navigateToAttendance();
    }

    interface CheckinPresenter {
        void loadPresentChild();
        void loadMessage();
        void clockInChild();
        void loadPickUpAdults();
        void updateUI(boolean activatedSwitch);
        void onConnectionFailedTapped();
        void onClockInFailedTapped();
        void onDestroy();
    }
}
