package se.home.nesho.poodle.overview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.menu.MenuActivity;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.overview.overviewchild.OverviewChildActivity;
import se.home.nesho.poodle.util.Constants;


/**
 * Created by Rr on 2017-02-20.
 * This activity contains list with all the children from the current selected section.
 * When ever the teacher clicks one of the children in the list they get to a new activity.
 */

public class OverviewActivity extends Activity implements OverviewMvp.OverviewView, OverViewAdapter.OnChildItemTappedListener {

    private OverViewAdapter adapter;
    private ProgressBar progressBar;
    private OverviewMvp.OverviewPresenter presenter;
    private RecyclerView recyclerView;
    private TextView sectionTitle;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);
        this.presenter = new OverviewPresenterImpl(this);

        //XML
        this.recyclerView = (RecyclerView)findViewById(R.id.overview_child_list);
        this.progressBar = (ProgressBar)findViewById(R.id.overview_loading_spinner);
        this.sectionTitle = (TextView)findViewById(R.id.overview_section_title_name);

        this.adapter = new OverViewAdapter(this);
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(adapter);
        this.sectionTitle.setText(Constants.CURRENT_SECTION);

        this.presenter.loadChildren();
    }

    /**
     * All loaded children.
     * */
    @Override
    public void setChildren(List<Child> data) {
        this.adapter.setItems(data);
    }

    /**
     * Show progressbar.
     * */
    @Override
    public void showProgress() {
        this.progressBar.setVisibility(View.VISIBLE);
        this.recyclerView.setVisibility(View.INVISIBLE);
    }

    /**
     * Hide progressbar.
     * */
    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
    }

    /**
     * When selecting child from list.
     * */
    @Override
    public void onChildTapped(Child child) {
        this.startActivity(OverviewChildActivity.newIntent(this, child.getSocialSecurityNumber()));
    }

    /**
     * When trying to get children by retrofit , fail show dialog
     * so user can retry again.
     * */
    @Override
    public void showConnectionFailed() {
        this.hideProgress();
        Snackbar snackbar = Snackbar.make(this.findViewById(R.id.activity_overview_container),getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> this.presenter.onRetryTapped());
        snackbar.show();
    }


    /**
     * Go back to menu activity.
     * */
    @Override
    public void navigateToMenu() {
        this.startActivity(new Intent(this, MenuActivity.class));

    }

    /**
     * Message when there is no children's in the current section.
     * */
    @Override
    public void errorNoChildrenFound() {
        Toast.makeText(this,getString(R.string.overview_error_noChildrenFound),Toast.LENGTH_LONG).show();
    }

    /**
     * When user clicks back button.
     * */
    @Override
    public void onBackPressed() {
        this.presenter.onBackPressedTapped();
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }

}


