package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho & Rr on 2017-01-23.
 */

public class AdultRestController {

    public void getAllAdults(final OnAdultsLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AdultService service = retrofit.create(AdultService.class);
        Call<List<Adult>> call = service.getAllAdults();

        call.enqueue(new Callback<List<Adult>>() {
            @Override
            public void onResponse(Call<List<Adult>> call, Response<List<Adult>> response) {
                List<Adult> adults = response.body();
                callback.onAdultsLoaded(adults);
                for (Adult adult : response.body()) {
                    Log.d("Retrofit Success: ", adult.getFirstName());
                }
            }

            @Override
            public void onFailure(Call<List<Adult>> call, Throwable t) {
                callback.onAdultsLoadedFailure(t);
                Log.d("Retrofit Failure: " , t.getMessage());
            }
        });
    }

    public void getAdult(String socialSecurityNumber , OnAdultLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AdultService service = retrofit.create(AdultService.class);
        Call<Adult> call = service.getAdult(socialSecurityNumber);

       call.enqueue(new Callback<Adult>() {
           @Override
           public void onResponse(Call<Adult> call, Response<Adult> response) {
               Adult adult = response.body();

               if (adult != null) {
                   callback.onAdultLoaded(adult);
               } else {
                   Log.d("Retrofit Error","adult is null");
               }
           }

           @Override
           public void onFailure(Call<Adult> call, Throwable t) {
                callback.onAdultLoadedFailure(t);
               Log.d("Retrofit Failure: ", t.getMessage());
           }
       });
    }

    public void postNewAdult(String childSSN,Adult adult, OnPostNewAdultCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AdultService service = retrofit.create(AdultService.class);
        Call<Adult> call = service.postAdult(childSSN,adult);

        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onPostNewAdultLoaded();
                } else {
                    Log.d("Retrofit","Something went wrong on posting, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                callback.onPostNewAdultLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to POST new adult," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void editAdult(Adult adult, OnEditAdultCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AdultService service = retrofit.create(AdultService.class);
        Call<Adult> call = service.editAdult(adult.getSocialSecurityNumber(),adult);

        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onEditAdultLoaded();
                } else {
                    Log.d("Retrofit","Something went wrong on editing adult, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                callback.onEditAdultLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to edit adult," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }

    public void removeAdult(Adult adult, OnRemoveAdultCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AdultService service = retrofit.create(AdultService.class);
        Call<Adult> call = service.removeAdult(adult);

        call.enqueue(new Callback<Adult>() {
            @Override
            public void onResponse(Call<Adult> call, Response<Adult> response) {
                if(response.isSuccessful() && response.code() == 204) {
                    callback.onRemoveAdultLoaded();
                    Log.d("Retrofit Sucess","Adult removed");
                } else {
                    Log.d("Retrofit","Something went wrong on remove adult, Response code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Adult> call, Throwable t) {
                callback.onRemoveAdultLoadedFailure(t);
                Log.d("Retrofit Fail","Fail to remove adult," + t.getMessage() + "  URL:" + retrofit.baseUrl());
                t.printStackTrace();
            }
        });
    }


    public interface OnAdultsLoadedCallback {
        void onAdultsLoaded(List<Adult> adults);
        void onAdultsLoadedFailure(Throwable t);
    }

    public interface OnAdultLoadedCallback {
        void onAdultLoaded(Adult adult);
        void onAdultLoadedFailure(Throwable t);
    }

    public interface OnPostNewAdultCallback {
        void onPostNewAdultLoaded();
        void onPostNewAdultLoadedFailure(Throwable t);
    }

    public interface OnEditAdultCallback {
        void onEditAdultLoaded();
        void onEditAdultLoadedFailure(Throwable t);
    }

    public interface OnRemoveAdultCallback {
        void onRemoveAdultLoaded();
        void onRemoveAdultLoadedFailure(Throwable t);
    }
}
