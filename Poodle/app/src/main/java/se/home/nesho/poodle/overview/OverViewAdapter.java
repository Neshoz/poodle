package se.home.nesho.poodle.overview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Rr on 2017-03-25.
 */

public class OverViewAdapter extends RecyclerView.Adapter<OverViewAdapter.ViewHolder>{


    private List<Child> childrend;
    private OnChildItemTappedListener onTappListiner;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView childName,childSSN;

        public ViewHolder(View itemView) {
            super(itemView);
            this.childName = (TextView)itemView.findViewById(R.id.overview_list_item_child_name);
            this.childSSN = (TextView)itemView.findViewById(R.id.overview_list_item_child_ssn);
        }
    }

    public OverViewAdapter(OnChildItemTappedListener onTappListiner) {
        this.childrend = new ArrayList<>();
        this.onTappListiner = onTappListiner;
    }

    public OverViewAdapter() {
        this.childrend = new ArrayList<>();
    }


    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.overview_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Child child = this.childrend.get(position);

        holder.childName.setText(this.childrend.get(position).getFirstName() + " " + this.childrend.get(position).getLastName());
        holder.childSSN.setText(this.childrend.get(position).getSocialSecurityNumber());
        holder.itemView.setTag(child);

        /**
         * Click listener when selecting a child from list.
         * */
        holder.itemView.setOnClickListener(view -> {
            Child selectedChild = (Child)view.getTag();
            onTappListiner.onChildTapped(selectedChild);

        });
    }

    @Override
    public int getItemCount() {
        return this.childrend.size();
    }

    public void setItems(List<Child> data) {
        this.childrend.clear();
        this.childrend.addAll(data);
        this.notifyDataSetChanged();
    }

    /**
     * Callback for itemView tapped.
     * */
    public interface OnChildItemTappedListener {
        void onChildTapped(Child child);
    }
}
