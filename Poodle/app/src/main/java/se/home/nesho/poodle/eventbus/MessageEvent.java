package se.home.nesho.poodle.eventbus;

/**
 * Created by Nesho on 2017-02-20.
 */

public class MessageEvent {

    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
