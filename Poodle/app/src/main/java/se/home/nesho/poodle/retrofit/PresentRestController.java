package se.home.nesho.poodle.retrofit;

import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.home.nesho.poodle.model.ClockOut;
import se.home.nesho.poodle.model.Present;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-23.
 */

public class PresentRestController {

    public void getPresentChildrenInSection(String date, String section, OnPresentLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PresentService service = retrofit.create(PresentService.class);
        Call<List<Present>> call = service.getPresentChildrenInSection(date, section);

        call.enqueue(new Callback<List<Present>>() {
            @Override
            public void onResponse(Call<List<Present>> call, Response<List<Present>> response) {
                List<Present> present = response.body();
                callback.onPresentLoaded(present);
                Log.d("Retrofit Success:", "Success fetching present children in section - " + present.size());
            }

            @Override
            public void onFailure(Call<List<Present>> call, Throwable t) {
                callback.onPresentLoadedFailure(t);
                Log.d("Retrofit Failure: ", "Failure in fetching present children in section - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getPresentChild(String date, String section, long socialSecurityNumber, OnPresentChildLoadedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PresentService service = retrofit.create(PresentService.class);
        Call<Present> call = service.getPresentChild(date, section, socialSecurityNumber);
        call.enqueue(new Callback<Present>() {
            @Override
            public void onResponse(Call<Present> call, Response<Present> response) {
                Present data = response.body();
                callback.onPresentChildLoaded(data);
                Log.d("Retrofit Success", "Success fetching present child - " + response.code());
            }

            @Override
            public void onFailure(Call<Present> call, Throwable t) {
                callback.onPresentChildLoadedFailure(t);
                Log.d("Retrofit failure", "Failure fetching present child - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void clockOutChild(String date, long socialSecurityNumber, ClockOut clockOutTime, OnChildClockedOutCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PresentService service = retrofit.create(PresentService.class);
        Call<ClockOut> call = service.clockOutChild(date, socialSecurityNumber, clockOutTime);

        call.enqueue(new Callback<ClockOut>() {
            @Override
            public void onResponse(Call<ClockOut> call, Response<ClockOut> response) {
                callback.onChildClockedOut();
                Log.d("Retrofit Success: ", "Successfully clocked out child - " + response.code());
            }

            @Override
            public void onFailure(Call<ClockOut> call, Throwable t) {
                callback.onChildClockedOutFailure(t);
                Log.d("Retrofit Failure: ", "Failed to clock out child - " + t.getMessage());
            }
        });
    }

    public void postPresentAttendance(Present present, OnPresentPostedCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PresentService service = retrofit.create(PresentService.class);
        Call<Present> call = service.postPresentAttendance(present);

        call.enqueue(new Callback<Present>() {
            @Override
            public void onResponse(Call<Present> call, Response<Present> response) {
                if(response.isSuccessful()) {
                    callback.onPresentPosted();
                    Log.d("Retrofit success", "Successfully posted present attendance - " + response.code());
                }
            }

            @Override
            public void onFailure(Call<Present> call, Throwable t) {
                callback.onPresentPostedFailure(t);
                Log.d("Retrofit Failure", "Failed posting present attendance - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void getAllPresentChildren(String date, OnGetAllPresentChildrenCallback callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PresentService service = retrofit.create(PresentService.class);
        Call<List<Present>> call = service.getAllPresentChildren(date);

        call.enqueue(new Callback<List<Present>>() {
            @Override
            public void onResponse(Call<List<Present>> call, Response<List<Present>> response) {
                List<Present> present = response.body();
                callback.onGetAllPresentChildrenLoaded(present);
                Log.d("Retrofit Success:", "Success fetching all presents - " + present.size());
            }

            @Override
            public void onFailure(Call<List<Present>> call, Throwable t) {
                callback.onGetAllPresentChildrenLoadedFailure(t);
                Log.d("Retrofit Failure: ", "Failure in fetching all presents - " + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public interface OnPresentLoadedCallback {
        void onPresentLoaded(List<Present> present);
        void onPresentLoadedFailure(Throwable t);
    }

    public interface OnChildClockedOutCallback {
        void onChildClockedOut();
        void onChildClockedOutFailure(Throwable t);
    }

    public interface OnPresentChildLoadedCallback {
        void onPresentChildLoaded(Present data);
        void onPresentChildLoadedFailure(Throwable t);
    }

    public interface OnPresentPostedCallback {
        void onPresentPosted();
        void onPresentPostedFailure(Throwable t);
    }

    public interface OnGetAllPresentChildrenCallback {
        void onGetAllPresentChildrenLoaded(List<Present> present);
        void onGetAllPresentChildrenLoadedFailure(Throwable t);
    }
}
