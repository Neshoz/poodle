package se.home.nesho.poodle.checkin;

import android.util.Log;
import android.view.View;

import java.util.List;

import se.home.nesho.poodle.model.Adult;
import se.home.nesho.poodle.model.Attendance;
import se.home.nesho.poodle.model.Child;
import se.home.nesho.poodle.model.ChildMessage;
import se.home.nesho.poodle.model.Present;
import se.home.nesho.poodle.retrofit.AttendanceRestController;
import se.home.nesho.poodle.retrofit.ChildRestController;
import se.home.nesho.poodle.util.AppUtils;
import se.home.nesho.poodle.util.Constants;

/**
 * Created by Nesho on 2017-02-15.
 */

public class CheckinPresenterImpl implements CheckinMvp.CheckinPresenter {
    private static final String TAG = "CheckinPresenterImpl";

    private CheckinMvp.CheckinView view;
    private ChildRestController childRestController;
    private AttendanceRestController attendanceRestController;

    public CheckinPresenterImpl(CheckinMvp.CheckinView view) {
        this.view = view;
        this.childRestController = new ChildRestController();
        this.attendanceRestController = new AttendanceRestController();
    }

    @Override
    public void loadPresentChild() {
        this.view.showGetChildProgress();
        this.childRestController.getChild(this.view.getChildSSN(), new ChildRestController.onChildLoadedCallback() {
            @Override
            public void onChildLoaded(Child child) {
                view.hideGetChildProgress();
                view.setChild(child);
            }

            @Override
            public void onChildLoadedFailure(Throwable t) {
                view.hideGetChildProgress();
                view.showConnectionFailed();
            }
        });
    }

    @Override
    public void loadMessage() {
        this.attendanceRestController.getMessage(this.view.getChildSSN(), new AttendanceRestController.OnMessageLoadedCallback() {
            @Override
            public void onMessageLoaded(ChildMessage message) {
                if(message != null) {
                    view.setMessage(message);
                }
            }

            @Override
            public void onMessageLoadedFailure() {

            }
        });
    }

    /**
     * This can be optimized af.
     */
    @Override
    public void clockInChild() {
        if(this.view.getSelectedAdult() == null) {
            this.view.setAdultPickUpError();
        }
        else {
            final Attendance attendance = new Attendance(this.view.getChild(), this.view.getSelectedAdult(), Constants.LOGGED_IN_TEACHER, AppUtils.getCurrentDate());
            final ChildMessage message = new ChildMessage(this.view.getChild(), this.view.getMessage());
            Log.d(TAG, "child name: " + message.getChild().getFirstName() + " message: " + message.getMessage());
            Log.d(TAG, "Adult firstName - " + attendance.getAdult().getFirstName());
            this.view.showClockInProgress();
            this.attendanceRestController.clockIn(new Present(this.view.getChild(), AppUtils.getCurrentDate(), AppUtils.getCurrentTime()), new AttendanceRestController.OnClockInCallback() {
                @Override
                public void onClockInSuccess() {
                    attendanceRestController.postAttendance(attendance, new AttendanceRestController.OnPostAttendanceCallback() {
                        @Override
                        public void onAttendancePosted() {
                            attendanceRestController.postOrUpdateMessage(message, new AttendanceRestController.OnMessagePostedCallback() {
                                @Override
                                public void onPostSuccess() {
                                    view.hideClockInProgress();
                                    view.navigateToAttendance();
                                }

                                @Override
                                public void onPostFailure() {
                                    view.hideClockInProgress();
                                    view.showClockInFailed();
                                }
                            });
                        }

                        @Override
                        public void onAttendancePostedFailure(Throwable t) {
                            view.hideClockInProgress();
                            view.showClockInFailed();
                        }
                    });
                }

                @Override
                public void onClockInFailure(Throwable t) {
                    view.hideClockInProgress();
                    view.showClockInFailed();
                }
            });
        }
    }

    @Override
    public void loadPickUpAdults() {
        this.childRestController.getAllowedPickUpAdults(this.view.getChildSSN(), new ChildRestController.OnPickUpAdultsLoadedCallback() {
            @Override
            public void onAdultsLoaded(List<Adult> adults) {
                view.setPickUpAdults(adults);
            }

            @Override
            public void onAdultsLoadedFailure() {
                view.showAdultsLoadedFailed();
            }
        });
    }

    @Override
    public void updateUI(boolean activatedSwitch) {
        if(activatedSwitch) {
            this.makeUIVisible();
        }
        else {
            this.makeUIInvisible();
        }
    }

    @Override
    public void onConnectionFailedTapped() {
        this.loadPresentChild();
    }

    @Override
    public void onClockInFailedTapped() {
        this.clockInChild();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    private void makeUIVisible() {
        this.view.getContainerLayout().setVisibility(View.VISIBLE);
        this.view.getPickUpRowLayout().setVisibility(View.VISIBLE);
        this.view.getClockInButton().setVisibility(View.VISIBLE);
        this.view.getMessageInputLayout().setVisibility(View.VISIBLE);
    }

    private void makeUIInvisible() {
        this.view.getContainerLayout().setVisibility(View.GONE);
        this.view.getPickUpRowLayout().setVisibility(View.GONE);
        this.view.getClockInButton().setVisibility(View.GONE);
        this.view.getMessageInputLayout().setVisibility(View.GONE);
    }
}
