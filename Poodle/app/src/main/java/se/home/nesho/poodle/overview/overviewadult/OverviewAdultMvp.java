package se.home.nesho.poodle.overview.overviewadult;

import android.app.AlertDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.widget.TextView;

import se.home.nesho.poodle.model.Adult;

/**
 * Created by Rr on 2017-04-01.
 * Get used in OverviewAdultFragment & OverviewAdultImpl.
 */

public interface OverviewAdultMvp {

    interface OverviewAdultView {

        //Get fields and buttons
        TextInputEditText getSSNField();
        TextInputEditText getFirstNameField();
        TextInputEditText getLastNameField();
        TextInputEditText getPhoneNumberField();
        TextInputEditText getWorkNumberField();
        TextInputEditText getEmailField();
        TextView getNameTitle();
        AppCompatSpinner getRelationSpinner();
        FloatingActionButton getRemoveButton();
        FloatingActionButton getSaveButton();
        FloatingActionButton getEditButton();
        void fillInFieldsWithInformation(Adult adult);

        //Error
        void clearAllErrorMessages();
        void showFirstNameError();
        void showLastNameError();
        void showPhoneNumberError();
        void showWorkNumberError();
        void showEmailError();
        void showFailToSaveError();
        void showFailToRemoveAdult();


        void showUpdatingInformationProgress();
        void showRemovingAdultProgress();

        void hideProgress();
        void showReadingInformationProgress();
        void failToReadAdultError();


        //Functionality
        AlertDialog showRemoveDialog();
        AlertDialog showOnBackPressedDialog();
        void navigateToChildOverview();

    }

    interface OverviewAdultPresenter {
        void onLoadAdultInformation(String adultSSN);
        void removeAdult();
        void onRemoveButtonTapped();
        void onSaveButtonTapped();
        void onEditButtonTapped();
        void onBackPressedPressed();
        void onDestroy();
    }
}
