package se.home.nesho.poodle.attendance.absent;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import se.home.nesho.poodle.R;
import se.home.nesho.poodle.checkin.CheckinActivity;
import se.home.nesho.poodle.model.Child;

/**
 * Created by Nesho on 2017-02-17.
 */

public class AbsentFragment extends Fragment implements AbsentMvp.AbsentView, AbsentAdapter.OnChildClickedListener {

    private AbsentMvp.AbsentPresenter presenter;
    private RecyclerView recyclerView;
    private AbsentAdapter adapter;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(this.getContext()).inflate(R.layout.fragment_absent, container, false);

        this.recyclerView = (RecyclerView) view.findViewById(R.id.absent_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        this.adapter = new AbsentAdapter(this.getContext(), this);

        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(adapter);

        this.presenter = new AbsentPresenterImpl(this);
        this.presenter.loadAbsentAttendance();

        return view;
    }

    @Override
    public void setAttendance(List<Child> data) {
        this.adapter.setItems(data);
    }

    @Override
    public void showProgress() {
        this.progressDialog = ProgressDialog.show(this.getActivity(), this.getString(R.string.progressDialogTitle), this.getString(R.string.progressDialogMessage), false);
    }

    @Override
    public void hideProgress() {
        this.progressDialog.dismiss();
    }

    @Override
    public void showConnectionFailed() {
        Snackbar snackbar = Snackbar.make(this.getActivity().findViewById(R.id.activity_attendance), this.getString(R.string.connectionFailedMessage), Snackbar.LENGTH_INDEFINITE)
                .setAction(this.getString(R.string.retryConnection), view -> presenter.onErrorMessageRetryTapped());
        snackbar.show();
    }

    @Override
    public void navigateToCheckIn(Child child) {
        this.startActivity(CheckinActivity.newIntent(this.getActivity(), child.getSocialSecurityNumber()));
    }

    @Override
    public void onChildClicked(Child child) {
        this.presenter.onChildTapped(child);
    }

    @Override
    public void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
